(function () {
   window.AddToGroup  = (id) => {
    Send({command: "show-create-group"})
  }
  window.addFrameDefault = () =>{
    Send({command: 'show-add-frame', value : {x: 1, y: 1, width: 2, height: 2, page: currentPage}})
  }
  window.shareDapp  = (id) => {
    Send({command: "share-d-app", value: {content: id}})
  }
  window.showMenuFrame = () => {
    const eleMenu = document.getElementById('menuframe_list')
    if(eleMenu.classList.contains('hidden')){
      document.getElementById('btn-category').innerHTML = window.closeIcon;
      eleMenu.classList.remove('hidden');
    }else {
      document.getElementById('btn-category').innerHTML = iconCategory;
      eleMenu.classList.add('hidden');
    }
  }
  window.AddPage = () => {
    window.showMenuFrame();
    pages.push([])
    currentPage = pages.length;
    setPages()
    mappingValues[currentPage] = []
    setMappingValues();
    window.localStorage.setItem('currentPage', currentPage)
    window.location.reload()
    // scrollTo(((pages.length - 1) * windowWidth))
    // renderVirtualPrev();
    // renderNumberNavigation()
    // renderGridMain()
  }
  window.resetDataEdit = () => {
    window.localStorage.removeItem('mappingValues')
    window.localStorage.removeItem('mappingFooterValues')
    window.localStorage.removeItem('pages')
    pages = JSON.parse(window.localStorage.getItem('initial-pages'))
    mappingValues = {}
    mappingFooterValues =  {}
    isEdit = false;
    renderVirtualPrev()
    toggleShowBarController()
    renderNumberNavigation();
  }
  window.handleDoneEdit = function (){
    let data = null;
    for (let n in mappingValues) {
      for (let t in mappingValues[n]) {
          if(mappingValues[n][t].notInSafe){
            data = t;
            break;
          }
      }
      if(data){
        break;
      }
    }
    // for(let i = 0; i < pages.length; i++){
    //   for(let j = 0; j < pages[i].length; j++){
    //     if(pages[i][j].id == value.idFrame){
    //       data = pages[i][j];
    //       break;
    //     }
    //   }
    //   if(data){
    //     break;
    //   }
    // }
    if(!data){
     Send({command: 'update-all-d-app', value: pages})
     window.localStorage.setItem('initial-pages', JSON.stringify(pages))
     toggleEdit()
      return;
    }
    currentPage = parseInt(getPage(data))
    scrollTo(((currentPage - 1) * windowWidth))
    renderVirtual()
  }
  window.openEdit = function (){
    toggleEdit()
    renderNumberNavigation(true)
  }
  const handleShowLabel = (name, width) => {
    let widthSvg = 5 * name.length + 26;
    const bool = widthSvg > width - 10
    widthSvg = bool? width - 10: widthSvg;
    const svg = `<svg
      width=${widthSvg}
      height="15"
      viewBox="0 0 ${widthSvg} 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <path
        d="M0 15C0 6.71573 6.71573 0 15 0H${widthSvg}V0C${widthSvg} 8.28427 ${widthSvg - 15 / 2} 15 ${widthSvg - 15} 15H0V15Z"
        fill="#FFDF00"
      />
    </svg>`
    return`<div class="label">
      ${svg}
      <span style="
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        padding-left: 10px;
        width: ${bool? "95%": "100%"};
      ">${name}</span>
    </div>`
  }
  function parseLocalStorage(key, type, initValue) {
    const strObj = localStorage.getItem(key);
    let obj = false;
    if (typeof strObj === 'string') {

      if (['array', 'object'].indexOf(type) > -1) {
        try {
          obj = JSON.parse(strObj);
        } catch (e) {
          obj = false;
        }
      } else if (type === 'number') {
        try {
          obj = strObj * 1;
        } catch (e) {
          obj = false;
        }
      }
    }
    if (type === 'array' && Array.isArray(obj)) {
      return [...obj]
    } else if (type === 'object' && typeof obj === 'object' && obj !== null) {
      return { ...obj }
    } else if (type === 'string' && typeof strObj === 'string') {
      return strObj;
    } else if (type === 'number' && typeof obj === 'number' && !isNaN(obj)) {
      return obj;
    }

    if (type === 'array' && Array.isArray(initValue)) {
      return [...initValue]
    } else if (type === 'object' && typeof initValue === 'object' && initValue !== null) {
      return { ...initValue }
    } else if (type === 'string' && typeof initValue === 'string') {
      return initValue;
    } else if (type === 'number' && typeof initValue === 'number' && !isNaN(initValue)) {
      return initValue;
    }

    if (type === 'array') {
      return [];
    } else if (type === 'object') {
      return {};
    } else if (type === 'string') {
      return '';
    } else if (type === 'number') {
      return 0;
    }

    return null;
  }
  var pages = parseLocalStorage('pages', 'array', window.pages)
  

  var itemInFooters = parseLocalStorage('itemInFooters', 'array', window.itemInFooters)



  if (typeof window !== 'object' || window === null) {
    alert('error: can not define window')
    return;
  }
  if (typeof window.webkit !== 'object' || window.webkit === null) {
    window.webkit = {}
  }
  if (typeof window.webkit.messageHandlers !== 'object' || window.webkit.messageHandlers === null) {
    window.webkit.messageHandlers = {}
  }
  if (typeof window.webkit.messageHandlers.callbackHandler !== 'object' || window.webkit.messageHandlers.callbackHandler === null) {
    window.webkit.messageHandlers.callbackHandler = {}
  }

  // let isStandalone = false;

  let isDebug = true;
  let isStandalone = true;
  if (window.navigator.standalone) {
    // user navigated from web clip, don't redirect
    isStandalone = true;
  }

  if (!(/Android|iPhone/i.test(navigator.userAgent))) {
    isStandalone = true;
  }
  if (typeof window.location.search === 'string' && window.location.search.indexOf('debug') > -1) {
    isStandalone = true;
    isDebug = true;
  }

  // if (!isDebug) {
  //   console.log = function () { }
  // }

  if (typeof window.webkit.messageHandlers.callbackHandler.postMessage !== 'function') {
    window.webkit.messageHandlers.callbackHandler.postMessage = console.log
  }
  var isEdit = parseLocalStorage('isEdit', 'number', 0) ? true : false;
  /*
let isFullscreen = false;
function fullscreen (e) {

  console.log('--------fullscreen')
  e.preventDefault();
  var elem = document.body;
  function openFullscreen() {
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.webkitRequestFullscreen) {
      elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) {
      elem.msRequestFullscreen();
    }
    isFullscreen = true;
  }

  function closeFullscreen() {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    }
    isFullscreen = false;
  }

  if (isFullscreen) {
    closeFullscreen();
  } else {
    openFullscreen();
  }
}
*/

  Math.easeInOutQuad = function (t, b, c, d) {
    t /= d / 2;
    if (t < 1) {
      return c / 2 * t * t + b
    }
    t--;
    return -c / 2 * (t * (t - 2) - 1) + b;
  };

  Math.easeInCubic = function (t, b, c, d) {
    var tc = (t /= d) * t * t;
    return b + c * (tc);
  };

  Math.inOutQuintic = function (t, b, c, d) {
    var ts = (t /= d) * t,
      tc = ts * t;
    return b + c * (6 * tc * ts + -15 * ts * ts + 10 * tc);
  };

  // requestAnimationFrame for Smart Animating http://goo.gl/sx5sts
  var requestAnimFrame = (function () {
    return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function (callback) { window.setTimeout(callback, 1000 / 60); };
  })();

  function scrollTo(to, callback, duration) {
    // because it's so fucking difficult to detect the scrolling element, just move them all
    function move(amount) {

      if (isPortrait) {
        const amountNative = amount - (currentPage - 1) * window.innerWidth;
        moveFrame(amountNative * -1, 0);
        document.getElementById('main').scrollLeft = amount;
      } else {
        const amountNative = amount - (currentPage - 1) * window.innerHeight;
        moveFrame(0, amountNative * -1);
        document.getElementById('main').scrollTop = amount;
      }
    }
    function position() {
      if (isPortrait) {
        return document.getElementById('main').scrollLeft;
      } else {
        return document.getElementById('main').scrollTop;
      }
    }
    var start = position(),
      change = to - start,
      currentTime = 0,
      increment = 20;
    duration = (typeof (duration) === 'undefined') ? 500 : duration;
    var animateScroll = function () {
      // increment the time
      currentTime += increment;
      // find the value with the quadratic in-out easing function
      var val = Math.easeInOutQuad(currentTime, start, change, duration);
      // move the document.body
      move(val);
      // do the animation unless its over
      if (currentTime < duration) {
        requestAnimFrame(animateScroll);
      } else {
        if (callback && typeof (callback) === 'function') {
          // the animation is done so lets callback
          callback();
        }
      }
    };
    animateScroll();
  }



  // --------------------

  const moveFrame = function (x, y) {
    window.webkit.messageHandlers.callbackHandler.postMessage(
      JSON.stringify({
        command: 'frame',
        action: 'move',
        content: {
          page: currentPage,
          x,
          y,
        }
      })
    );


  }
  const hidePageInFrame = function (page) {
    window.webkit.messageHandlers.callbackHandler.postMessage(
      JSON.stringify({
        command: 'frame',
        action: 'hide',
        content: {
          page: currentPage,
        }
      })
    );


  }
  const activePageInFrame = function (page) {
    window.webkit.messageHandlers.callbackHandler.postMessage(
      JSON.stringify({
        command: 'frame',
        action: 'show',
        content: {
          page,
        }
      })
    );


  }

  const removeAllFrame = function () {
    window.webkit.messageHandlers.callbackHandler.postMessage(
      JSON.stringify({
        command: 'frame',
        action: 'remove',
        content: {
        }
      })
    );


  }

  const hideLabelFrame = function () {
    window.webkit.messageHandlers.callbackHandler.postMessage(
      JSON.stringify({
        command: 'frame',
        action: 'touchDown',
        content: {
          page: currentPage,
        }
      })
    );


  }

  var isForceRenderVirtual = false;


  const isTouch = ('ontouchstart' in window);

  const bgs = ["red", "green", "blue", "brown", "yellow", "#506c84", "#c647b9", "#47c67f", "#c69947"];
  // document.getElementById('main').innerHTML = "";
  let numberColumn = 0;
  let numberRow = 0;

  let windowWidth = window.innerWidth, windowHeight = window.innerHeight;
  let isPortrait = false;

  var customItemInFooters = parseLocalStorage('customItemInFooters', 'object', {})

  var mappingDefaultValues = [];
  var mappingValues = {};

  var mappingFooterValues = parseLocalStorage('mappingFooterValues', 'object', {})

  var _target = null;

  var statusBarHeight = 30;
  var footerBarHeight = 0;
  if (typeof window.location.search === 'string' && window.location.search.indexOf('isApp') > -1) {
    footerBarHeight = 10;
  }

  var currentPage = parseLocalStorage('currentPage', 'number', 1)
  if (typeof currentPage !== 'number' || isNaN(currentPage) || currentPage > pages.length || currentPage < 1) {
    currentPage = 1
  }

  var itemWidth = 0; // quy dinh chieu ngang cua tung o
  var spaceWidth = 0; // quy dinh chieu ngang cua tung o

  var lastScrollLeft = 0;
  var fontSize = 11;
  var timeOutMuti = null;

  function setPageNumber(p = false) {
    if (typeof p !== 'number' || isNaN(p)) {
      p = currentPage;
    }

    p *= 1;
    if (isNaN(p)) {
      return;
    }

    console.log('-------save-page', p)
    localStorage.setItem('currentPage', p);

  }


  function setMappingValues() {

    if (typeof mappingValues !== 'object' || mappingValues === null) {
      return;
    }

    console.log('-------save-mappingValues')
    localStorage.setItem('mappingValues', JSON.stringify(mappingValues));
  }


  function setMappingFooterValues() {

    if (typeof mappingFooterValues !== 'object' || mappingFooterValues === null) {
      return;
    }

    console.log('-------save-mappingFooterValues')
    localStorage.setItem('mappingFooterValues', JSON.stringify(mappingFooterValues));
  }


  function setCustomItemInFooters() {

    if (typeof customItemInFooters !== 'object' || customItemInFooters === null) {
      return;
    }

    console.log('-------save-customItemInFooters')
    localStorage.setItem('customItemInFooters', JSON.stringify(customItemInFooters));
  }
  function setPages() {

    if (!Array.isArray(pages)) {
      return;
    }

    console.log('-------save-pages')
    localStorage.setItem('pages', JSON.stringify(pages));
  }

  function setItemInFooters() {

    if (!Array.isArray(itemInFooters)) {
      return;
    }

    console.log('-------save-itemInFooters')
    localStorage.setItem('itemInFooters', JSON.stringify(itemInFooters));
  }


  const checkRotate = function () {

    isPortrait = false;
    if (window.innerWidth < window.innerHeight) {
      isPortrait = true
    } else {
      isPortrait = false
    }

    render(isPortrait)

  }
  const resize = function () {
    console.log('resize-------------');

    // kiem tra lai portrai hay landscape
    if (
      (
        windowHeight == window.innerWidth && windowWidth == window.innerHeight
      ) ||
      (
        windowHeight == window.innerHeight && windowWidth == window.innerWidth
      )
    ) {

      cancelEdit();

      windowWidth = window.innerWidth
      windowHeight = window.innerHeight

      removeAllFrame();

      checkRotate();

      isLockChangePage = true;

      let space;
      if (isPortrait) {
        space = window.innerWidth;
      } else {
        space = window.innerHeight;
      }

      console.log('space', space, currentPage)


      scrollTo(((currentPage - 1) * space), function () {
        isLockChangePage = false
      }, 300);
      console.log('rotate');

      return;
    }
    if (isDebug) {
      alert('==========WARNING===========reload-------------');
    }
    window.location.reload();
  }
  checkRotate();


  let timeoutResize;
  window.addEventListener("resize", () => {
    document.getElementById('divOverlayResizing').style.display = 'block';

    clearTimeout(timeoutResize)
    timeoutResize = setTimeout(function () {
      resize();
    }, 250);

  });



  function render(isPortrait) {

    if (!isStandalone) {
      document.getElementById('divOverlayResizing').style.display = 'block';
      document.getElementById('divOverlayResizing').firstElementChild.innerHTML = "Please, open in Safari and add to Home screen";
      return;
    }

    removeAllFrame();

    mappingDefaultValues = [];
    mappingValues = parseLocalStorage('mappingValues', 'object', {})

    if (isPortrait) {

      windowWidth = window.innerWidth
      windowHeight = window.innerHeight

    } else {

      windowWidth = window.innerHeight
      windowHeight = window.innerWidth

    }


    if (windowWidth <= 120) {
      fontSize = 8;
      numberColumn = 1;
    } else if (windowWidth <= 220) {
      fontSize = 9;
      numberColumn = 2;
    } else if (windowWidth <= 320) {
      fontSize = 10;
      numberColumn = 3;
    } else if (windowWidth < 479) {
      fontSize = 11;
      numberColumn = 4;
    } else if (windowWidth < 767) {
      fontSize = 12;
      numberColumn = 5;
    } else if (windowWidth < 991) {
      fontSize = 14;
      numberColumn = 6;
    } else if (windowWidth < 1280) {
      fontSize = 15;
      numberColumn = 7;
    } else if (windowWidth < 1440) {
      fontSize = 16;
      numberColumn = 8;
    } else if (windowWidth < 1920) {
      fontSize = 28;
      numberColumn = 9;
    } else {
      fontSize = 34;
      numberColumn = 10;
    }



    // phan render



    let html = '';
    for (var i = 3; i < 51; i++) {


      itemWidth = (windowWidth - (numberColumn + 1) * i) / numberColumn
      // const space = ( windowWidth - ( 144 + 2 * i * 4 ) ) / 2

      // const n = (windowWidth - 108 + 2 * i) % 4
      if (itemWidth < 1 || itemWidth !== Math.floor(itemWidth)) {
        continue;
      }

      spaceWidth = i;
      break;
    }

    // tinh lai tong dong cho height
    const safeHeight = windowHeight - (itemWidth * 2 + spaceWidth) - statusBarHeight - footerBarHeight; // 20 la chieu cao statusBar, chua toi thieu 1.2 o 



    numberRow = Math.floor((safeHeight + spaceWidth) / (itemWidth + spaceWidth))

    // dao nguoc vi tri de xoay
    if (!isPortrait) {
      const tmp = numberRow;
      numberRow = numberColumn;
      numberColumn = tmp;

      document.getElementById("main").style.height = window.innerHeight + 'px';
      // document.getElementById("main").style.overflowY = 'scroll';

      document.getElementById("main").style.width = '100%';
      // document.getElementById("main").style.overflowX = 'hidden';


    } else {
      document.getElementById("main").style.width = window.innerWidth + 'px';
      // document.getElementById("main").style.overflowX = 'scroll';


      document.getElementById("main").style.height = '100%';
      // document.getElementById("main").style.overflowY = 'hidden';
    }




    // tinh tong chieu dai
    if (isPortrait) {

      window.allowDragWidth = numberColumn * (itemWidth + spaceWidth) + 10; // cho phep bien do let 10
      window.allowDragHeight = numberRow * (itemWidth + spaceWidth) + statusBarHeight + 10; // can them statusbar


      document.getElementById('content').style.width = (pages.length * windowWidth) + "px";
      document.getElementById('content').style.height = windowHeight + "px";
    } else {

      window.allowDragWidth = numberColumn * (itemWidth + spaceWidth) + statusBarHeight + 10; // cho phep bien do let 10
      window.allowDragHeight = numberRow * (itemWidth + spaceWidth) + 10; // can them statusbar

      document.getElementById('content').style.width = windowHeight + "px";
      document.getElementById('content').style.height = (pages.length * windowWidth) + "px";
    }
    console.log('numberRow', numberRow, 'numberColumn', numberColumn, 'spaceWidth', spaceWidth, "window.allowDragHeight", window.allowDragHeight, 'window.allowDragWidth', window.allowDragWidth)




    itemWidthWithBorder = itemWidth - 2;

    // render truoc o luoi gia, de chi add cac o nho vao  
    for (var p = 0; p < pages.length; p++) {

      if (!isPortrait) {
        html += `<div class="slide" id="slide-${p}" style="width: 100%;height:${windowWidth}px;">`;
      } else {
        html += `<div class="slide" id="slide-${p}" style="width: ${windowWidth}px;height:100%;float:left">`;
      }
      html += '</div>';
    }

    // ve them o chua du lieu
    console.log(itemInFooters)
    for (var i = 0; i < itemInFooters.length; i++) {

      const dataItem = itemInFooters[i];
      if (typeof dataItem !== 'object' || dataItem === null || Object.keys(dataItem).length === 0) {
        continue
      }

      let background = dataItem.backgroundColor;
      let content = '';

      const type = dataItem.type;
      const options = dataItem.options || {};

 
      console.log(dataItem)
      let x = dataItem.position.x * itemWidth + spaceWidth * (dataItem.position.x + 1);
      let y = window.innerHeight - itemWidth - footerBarHeight;


    
      let width = dataItem.position.width * (itemWidth) + (dataItem.position.width - 1) * spaceWidth;
      let height = dataItem.position.height * itemWidth + (dataItem.position.height - 1) * spaceWidth;




      if (!isPortrait) {
        const t = width;
        width = height;
        height = t;

        x = window.innerWidth - itemWidth - footerBarHeight;
        y = dataItem.position.x * itemWidth + spaceWidth * (dataItem.position.x + 1);

        // if (p === 0) {
        //   y = window.innerHeight - y - height;
        // }
      }

      // 
      const totalItem = 1000000 + i + 1;

      mappingFooterValues[totalItem] = {
        x, y, width, height
      };

      if (dataItem.type === 3) {
        window.webkit.messageHandlers.callbackHandler.postMessage(
          JSON.stringify({
            workerId: window.workerId,
            command: 'frame',
            action: 'register',
            content: {
              uuid: totalItem,
              name: dataItem.name,
              x,
              y,
              width,
              height,
              type,
              cornerRadius: options.cornerRadius,
              isAllowBackgroundMode: true,
              url: options.url,
              position: 'fixed',
            }
          })
        );
      }


      // const y = n * itemWidth + spaceWidth;


      let widthItem = Math.floor(width * 0.7) - 2; // pixel border moi canh la 1px
      const heightItem = widthItem

      const spaceItem = Math.floor((width - widthItem) / 4); // pixel border moi canh la 1px
      widthItem = width - spaceItem * 4;
      const pin = false;
      let contentBgFrame = ''
      if (dataItem.type == 1 || dataItem.type == 4) {

        let padding = Math.floor(width * 0.1);
        if (padding < 2) {
          padding = 2;
        }
        if (padding % 2 !== 0) {
          padding--;
        }

        content = renderHtmlDapp(dataItem, spaceItem, widthItem, width, fontSize);
      } else if (dataItem.type == 3) {
        contentBgFrame = renderHtmlFrame(dataItem);
        content = `${isEdit ? (pin?closePinSvg:pinSvg) +  editSvg + deleteSvg: ''}`;
      }

      let className = dataItem.type == 3?"item-frame": ''
      if (isEdit) {
        className += ` movable`
      } else {
        className += ` footer`
      }
   
      html += `<div class='${className}' id='movable-${totalItem}' data-footer="1" data-id="${dataItem.id}" data-type="${dataItem.type}" style='z-index: 1;position:fixed; left:${x}px; top:${y}px; width:${width}px; height:${height}px;'>
        <div class="warning"></div>
        ${contentBgFrame}
        <div class="content-item"  style='background: ${background}; width: ${width}px; height:${height}px;pointer-events: none;text-align: center;position: relative;overflow: hidden; border-radius: 18px'>
        ${content}
        </div>
        ${dataItem.type == 3 ? handleShowLabel(dataItem.name, width) + dataItem.content: "" }
    </div>`

    }
    document.getElementById('content').innerHTML = html;

    renderVirtual();


    scrollTo(((currentPage - 1) * windowWidth), function () {
      isLockChangePage = false
    }, 300);



    if (!isPortrait) {

      document.getElementById('pageNavigation').style.top = (window.innerHeight / 2 - itemWidth / 2) + "px";
      document.getElementById('pageNavigation').style.width = 83 + "px";
    


      document.getElementById('pageNavigationBar').style.top = 0;
      document.getElementById('pageNavigationBar').style.right = footerBarHeight + itemWidth + 'px';
      document.getElementById('pageNavigationBar').style.left = 'unset';
      document.getElementById('pageNavigationBar').style.bottom = 0;
      document.getElementById('pageNavigationBar').style.width = 100 + "px";
      document.getElementById('pageNavigationBar').style.height = "unset";
      document.getElementById('pageNavigation').style.lineHeight = itemWidth + "px";


      document.getElementById('footer').style.top = 0;
      document.getElementById('footer').style.right = 0;
      document.getElementById('footer').style.left = 'unset';
      document.getElementById('footer').style.bottom = 0;
      document.getElementById('footer').style.width = (itemWidth + spaceWidth) + "px";
      document.getElementById('btn-category').innerHTML = iconCategory
      toggleShowBarController()
    } else {
      document.getElementById('pageNavigation').style.top = 'unset';
      document.getElementById('pageNavigation').style.width = 83 + 'px';

      document.getElementById('pageNavigationBar').style.top = 'unset';
      document.getElementById('pageNavigationBar').style.right = 0;
      document.getElementById('pageNavigationBar').style.left = 0;
      document.getElementById('pageNavigationBar').style.bottom = footerBarHeight + itemWidth + 8 + 'px';
      document.getElementById('pageNavigationBar').style.width = "unset";
      document.getElementById('pageNavigationBar').style.height = 59 + "px";
      document.getElementById('pageNavigation').style.lineHeight = document.getElementById('pageNavigationBar').style.height;

      document.getElementById('footer').style.top = 'unset';
      document.getElementById('footer').style.right = 0;
      document.getElementById('footer').style.left = 0;
      document.getElementById('footer').style.bottom = footerBarHeight;
      document.getElementById('footer').style.height = (itemWidth + spaceWidth) + "px";

      document.getElementById('btn-category').innerHTML = iconCategory
      toggleShowBarController()
    }

    if (isEdit) {
      renderGridFooter();
    }

    setTimeout(function () {
      document.getElementById('divOverlayResizing').style.display = 'none';
    }, 100)
    activePageInFrame(currentPage);

    // setup event 
    window.toggleEdit = function (forceStatus) {

      // check isEdit
      if (isEdit) {
        const eles = document.getElementsByClassName("notInArea");
        if (eles.length > 0) {
          const ele = eles[0];
          console.log(ele);
          // lay page 
          let lastPage = -1;
          ele.classList.forEach(function (className) {
            if (className.indexOf("movable-") === -1) {
              return;
            }
            lastPage = className.replace("movable-", '') * 1 + 1;
          })
          if (confirm(`without save in  page ${lastPage} ?`) != true) {
            return;
          }

          cancelEdit();

          // window.location.reload();
          return;
        }
      }


      if (typeof forceStatus === 'undefined') {
        isEdit = !isEdit;
      } else {
        isEdit = forceStatus === true ? true : false;
      }

      localStorage.setItem('isEdit', isEdit ? 1 : 0);
      toggleShowBarController()
      // show hide icon frame
      const elementFrames = document.querySelectorAll('[data-type="3"]');
      elementFrames.forEach((ele) => {
        ele.getElementsByClassName('content-item')[0].innerHTML = isEdit ? pinSvg +  editSvg + deleteSvg: ''
      })

      // tien hanh an hoac hien edit
      if (isEdit) {
      
        removeAllFrame();

        renderGridMain();
        renderGridFooter();

        const eles = document.getElementsByClassName('footer')
        for (var i = eles.length - 1; i > -1; i--) {
          eles[i].classList.add('movable');
        }

        const eles2 = document.getElementsByClassName('notMovable')
        for (var i = eles2.length - 1; i > -1; i--) {
          eles2[i].classList.add('movable');
          eles2[i].classList.remove('notMovable')
        }

        const ele = document.getElementById('pageNavigation')
        if(ele){
          ele.onclick = (e) => {
            e.preventDefault();
            e.stopPropagation()
          }
        }
      } else {

        hideVirtual(currentPage - 2, true);
        hideVirtual(currentPage - 1, true);
        hideVirtual(currentPage, true);
        const ele = document.getElementById('pageNavigation')
        if(ele){
          ele.onclick = (e) => {
            e.preventDefault();
            e.stopPropagation()
            Send({command: 'show-search-home'})
          }

        }

        document.getElementById('footer').innerHTML = '';

        const eles = document.getElementsByClassName('footer')
        for (var i = eles.length - 1; i > -1; i--) {
          eles[i].classList.remove('movable')
        }

        const eles2 = document.getElementsByClassName('movable')
        for (var i = eles2.length - 1; i > -1; i--) {
          eles2[i].classList.add('notMovable');
          eles2[i].classList.remove('movable')
        }

        // dang ky native frame

        const realPage = currentPage - 1;

        let startPage = realPage - 1;
        if (startPage < 0) {
          startPage = 0;
        }
        hideVirtual(startPage - 1);

        let endPage = realPage + 2;
        if (endPage > pages.length) {
          endPage = pages.length;
        }
        hideVirtual(endPage);

        for (var p = startPage; p < endPage; p++) {
          const page = p + 1;

          const dataItems = pages[p];


          for (var i = 0; i < dataItems.length; i++) {


            const totalItem = page * 100000 + i + 1;


            const dataItem = dataItems[i];
            if (dataItem === null || typeof dataItem !== 'object') {
              continue;
            }


            const type = dataItem.type;
            const options = dataItem.options || {};


            let x, y, width, height, zIndex = 1;

            let xNative, yNative;

            let hasMappingValue = false;
            console.log(mappingValues, page, totalItem)
            if (typeof mappingValues[page][totalItem] !== 'object') {
              continue;
            }

            x = mappingValues[page][totalItem].x;
            y = mappingValues[page][totalItem].y;
            width = mappingValues[page][totalItem].width;
            height = mappingValues[page][totalItem].height;

            xNative = x - (p * window.innerWidth);
            yNative = y;



            if (dataItem.type === 3) {
              window.webkit.messageHandlers.callbackHandler.postMessage(
                JSON.stringify({
                  workerId: window.workerId,
                  command: 'frame',
                  action: 'register',
                  content: {
                    uuid: totalItem,
                    x: xNative,
                    y: yNative,
                    width,
                    height,
                    page,
                    type,
                    cornerRadius: options.cornerRadius,
                    isAllowBackgroundMode: options.isAllowBackgroundMode,
                    url: options.url,
                  }
                })
              );
            }
          }
        }

        // register footer native

        
        for (var i = 0; i < itemInFooters.length; i++) {
          
          const dataItem = itemInFooters[i];

          if (typeof dataItem !== 'object' || dataItem === null) {
            continue
          }

          const type = dataItem.type;
          const options = dataItem.options || {};

          // 
          const totalItem = 1000000 + i + 1;

          if (typeof mappingFooterValues[totalItem] !== 'object') {
            continue;
          }

          const { x, y, width, height } = mappingFooterValues[totalItem]

          if (dataItem.type === 3) {
            window.webkit.messageHandlers.callbackHandler.postMessage(
              JSON.stringify({
                workerId: window.workerId,
                command: 'frame',
                action: 'register',
                content: {
                  uuid: totalItem,
                  name: dataItem.name,
                  x,
                  y,
                  width,
                  height,
                  type,
                  cornerRadius: options.cornerRadius,
                  isAllowBackgroundMode: true,
                  url: options.url,
                  position: 'fixed',
                }
              })
            );
          }


        }
        // kich hoat page
        activePageInFrame(currentPage);


      }
    }
  }

  function renderGridMain() {

    const realPage = currentPage - 1;

    let startPage = realPage - 1;
    if (startPage < 0) {
      startPage = 0;
    }
    hideVirtual(startPage - 1);

    let endPage = realPage + 2;
    if (endPage > pages.length) {
      endPage = pages.length;
    }


    let isSaveMappingDefaultValue = false;
    if (mappingDefaultValues.length < 1) {
      isSaveMappingDefaultValue = true;
    }
    for (var p = startPage; p < endPage; p++) {

      let html = '', gridBorderHtml = '';

      // check xem da ton tai
      if (document.getElementById('slide-' + p).innerHTML !== '') {
        continue;
      }
      for (var n = 0; n < numberRow; n++) {
        let x, y;
        for (var i = 0; i < numberColumn; i++) {


          let background = bgs[i % 10];


          let xNative, yNative;

          xNative = i * itemWidth + spaceWidth * (i + 1);
          x = p * windowWidth + xNative;
          y = statusBarHeight + n * itemWidth + spaceWidth * (n + 1);

          yNative = y


          const width = itemWidthWithBorder;
          const height = itemWidthWithBorder;

          if (!isPortrait) {

            x = statusBarHeight + i * itemWidth + spaceWidth * (i + 1);

            yNative = n * itemWidth + spaceWidth * (n + 1);
            y = p * windowWidth + yNative;

            xNative = x;
            //   const t = x;
            //   x = y;
            //   y = t;
          }


          if (
            n === 0 &&
            (
              (i !== 0 && i !== numberColumn - 1)
              || (i === 0 && p !== startPage)
              || (i === numberColumn - 1 && p !== endPage)
            )
          ) {
            let className = 'grid';
            gridBorderHtml += `<div class="${className}" style='z-index: 1;position:absolute; left:${x - spaceWidth + spaceWidth * 0.25}px; top:${y}px; width:1px; height:${numberRow * (itemWidth + spaceWidth)}px;'>
        </div>`
          }
          // const y = n * itemWidth + spaceWidth;
          html += `<div class="display-center" id="grid-${i}-${n}" style='z-index: 1;position:absolute; left:${x}px; top:${y}px; width:${width}px; height:${height}px; border-radius: 5px'>
          <div class="add">${iconPlus}</div>
          </div>`

          if (isSaveMappingDefaultValue) {

            mappingDefaultValues.push({
              x: xNative,
              y: yNative, width, height, col: n, row: i,
            })
          }

        }


        if (n !== 0 && n !== numberRow) {
          let className = 'grid';
          gridBorderHtml += `<div class="${className}" style='z-index: 1;position:absolute; left:${p * window.innerWidth}px; top:${y - spaceWidth + spaceWidth * 0.25}px; width:${numberColumn * (itemWidth + spaceWidth)}px; height:1px;'>
    </div>`
        }
      }

      document.getElementById('slide-' + p).innerHTML = html + gridBorderHtml;
    }

    // // bat cho  tat ca icon rung trong 2 giay
    setTimeout(function () {
      const eles2 = document.getElementsByClassName('grid')
      for (var i = eles2.length - 1; i > -1; i--) {
        eles2[i].classList.remove('shake');
      }
    }, 1200)
  }
  function renderGridFooter() {
    let html = ''; let gridBorderHtml = ''
    let start = numberColumn;
    if (!isPortrait) {
      start = numberRow;
    }
    for (var i = 0; i < start; i++) {

      let x = i * itemWidth + spaceWidth * (i + 1);
      let y = footerBarHeight;

     

      // if (!isPortrait) {
      //   const t = x;
      //   x = y - footerBarHeight;
      //   y = t;
      // }
      // const y = n * itemWidth + spaceWidth;

      const width = itemWidthWithBorder;
      const height = itemWidthWithBorder;

      if (isPortrait) {
        extra = "left:" + x + "px; bottom:" + y  + "px";
      } else {
        extra = "right:" + footerBarHeight + "px; top:" + x + "px";
      }
      
      let className = 'grid';
            gridBorderHtml += `<div class="${className}" style='z-index: 1;position:absolute; left:${x - spaceWidth + spaceWidth * 0.25}px; top:${y}px; width:1px; height:${itemWidth - footerBarHeight}px;'>
        </div>`
        
      html += `<div class="display-center" style='z-index: 1;position:absolute; ${extra}; width:${width}px; height:${height}px; '>
      <div class="add">${iconPlus}</div>
    </div>`

    }
    document.getElementById('footer').innerHTML = html + gridBorderHtml;
  }
  function hideVirtual(p, isOnlyHideSlide = false, noHideGrid) {

    if (p < 0 || p >= pages.length) {
      return
    }
    if (document.getElementById('slide-' + p) === null) {
      return;
    }
    if(!noHideGrid || !isEdit){
      document.getElementById('slide-' + p).innerHTML = '';
    }
  

    if (isOnlyHideSlide === true) {
      return;
    }

    const eles = document.getElementsByClassName('movable-' + p);
    if (eles === null || eles.length < 1) {
      return
    }
    const parent = document.getElementById('content');
    for (var i = eles.length - 1; i >= 0; i--) {
      if(eles[i].dataset.pin == 1) return;
      parent.removeChild(eles[i]);
    }

  }
  var timeOutSearch = null;
  function renderNumberNavigation (noRenderSearch) {
    let html = ''
    timeOutSearch && clearTimeout(timeOutSearch);
    const ele = document.getElementById('pageNavigation');

    if(isMovePageNavigation){
      ele.innerHTML = html;
      return;
    }
    const pagelength = pages.length;
    if(currentPage === 1){
      html = `<span class="pageNavigation-item active">1</span>${pagelength === 1?"":`<span class="pageNavigation-item">2</span>`}`
    }else if(currentPage === pages.length){
      html = `<span class="pageNavigation-item">${pagelength - 1}</span><span class="pageNavigation-item active">${pagelength}</span>`
    }
    else {
      html = `<span class="pageNavigation-item">${currentPage - 1}</span><span class="pageNavigation-item active">${currentPage}</span><span class="pageNavigation-item">${currentPage + 1}</span>`
    }
    if(!noRenderSearch){
      timeOutSearch = setTimeout(() => {
        renderSearch()
      },1000)
    }
    ele.innerHTML = html
  }
  function renderSearch () {
    if(isEdit) return;
    const ele = document.getElementById('pageNavigation')
    ele.innerHTML = `<div id="btn-search" class="btn-search">${iconSearch} <span style="font-size: 11px">Search</span></div>`;
   
  }
  
  function renderVirtual() {

    hidePageInFrame(currentPage);
    let html = '';

    // document.getElementById('pageNavigation').innerHTML = `<span>${currentPage - 1}<span><span>${currentPage}<span><span>${currentPage + 1}<span>`;
    renderNumberNavigation(isEdit?true: false)
    const realPage = currentPage - 1;

    let startPage = realPage - 1;
    if (startPage < 0) {
      startPage = 0;
    }
    hideVirtual(startPage - 1);

    let endPage = realPage + 2;
    if (endPage > pages.length) {
      endPage = pages.length;
    }
    hideVirtual(endPage);



    if (isEdit) {
      renderGridMain();
    }

    const textHeight = 18;
    for (var p = startPage; p < endPage; p++) {

      const dataItems = pages[p];

      let isExistsRender = false;
      const eles = document.getElementsByClassName('movable-' + p);
      if (eles !== null && eles.length > 0) {
        if (!isForceRenderVirtual) {
          continue;
        }
        isExistsRender = true;
      }
      // break;

      const page = p + 1;

      if (typeof mappingValues[page] !== 'object' || mappingValues[page] === null) {
        mappingValues[page] = {};
      }


      for (var i = 0; i < dataItems.length; i++) {


        const totalItem = page * 100000 + i + 1;

        if (isExistsRender === true) {
          const ele = document.getElementById('movable-' + totalItem);
          if (ele !== null) {
            if (dataItems[i] === null) {
              ele.remove();
            }
            continue;
          }
        }

        let html = '';

        const dataItem = dataItems[i];
        if (dataItem === null || typeof dataItem !== 'object') {
          continue;
        }

        let background = dataItem.backgroundColor;
        let content = '';



        const type = dataItem.type;
        const options = dataItem.options || {};


        let x, y, width, height, zIndex = 1, notInSafe = false, pin = false;

        let xNative, yNative;

        let hasMappingValue = false;
        if (typeof mappingValues[page][totalItem] === 'object') {
          pin = mappingValues[page][totalItem].pin;
          hasMappingValue = true;
          x = pin?mappingValues[page][totalItem].x % window.innerWidth : mappingValues[page][totalItem].x;
          y = mappingValues[page][totalItem].y;
          width = mappingValues[page][totalItem].width;
          height = mappingValues[page][totalItem].height;
          zIndex = mappingValues[page][totalItem].zIndex;
          if (typeof zIndex !== 'number') {
            zIndex = 1;
          }
          notInSafe = mappingValues[page][totalItem].notInSafe;
        
        } else {

          xNative = dataItem.position.x * itemWidth + spaceWidth * (dataItem.position.x + 1);
          x = p * windowWidth + xNative;
          y = statusBarHeight + dataItem.position.y * itemWidth + spaceWidth * (dataItem.position.y + 1);
          yNative = y;

          width = dataItem.position.width * (itemWidth) + (dataItem.position.width - 1) * spaceWidth;
          height = dataItem.position.height * itemWidth + (dataItem.position.height - 1) * spaceWidth;
      
          if (!isPortrait) {
            const t = width;
            width = height;
            height = t;

            yNative = dataItem.position.x * itemWidth + spaceWidth * (dataItem.position.x + 1);
            y = p * window.innerHeight + yNative;

            x = statusBarHeight + dataItem.position.y * itemWidth + spaceWidth * (dataItem.position.y + 1);
            xNative = x;
            // if (p === 0) {
            //   y = window.innerHeight - y - height;
            // }
          }

      
          mappingValues[page][totalItem] = {
            x, y, width, height, type, options, id: dataItem.id
          }

          
          if (notInSafe !== true && dataItem.type === 3) {
            window.webkit.messageHandlers.callbackHandler.postMessage(
              JSON.stringify({
                workerId: window.workerId,
                command: 'frame',
                action: 'register',
                content: {
                  uuid: totalItem,
                  x: xNative,
                  y: yNative,
                  width,
                  height,
                  page,
                  type,
                  cornerRadius: options.cornerRadius,
                  isAllowBackgroundMode: options.isAllowBackgroundMode,
                  url: options.url,
                }
              })
            );
          }
        }




        // const y = n * itemWidth + spaceWidth;


        let widthItem = Math.floor(width * 0.7) - 2; // pixel border moi canh la 1px
        const heightItem = widthItem

        const spaceItem = Math.floor((width - widthItem) / 4); // pixel border moi canh la 1px
        widthItem = width - spaceItem * 4;
       
        let contentBgFrame = ''
        if (dataItem.type == 1 || dataItem.type == 4) {

          let padding = Math.floor(width * 0.1);
          if (padding < 2) {
            padding = 2;
          }
          if (padding % 2 !== 0) {
            padding--;
          }
          content = renderHtmlDapp(dataItem, spaceItem, widthItem, width, fontSize)

        } else if (dataItem.type == 3) {
          contentBgFrame = renderHtmlFrame(dataItem);
          content = `${isEdit ? (pin?closePinSvg:pinSvg) +  editSvg + deleteSvg: ''}`;
          
        }

        html += `<div class="warning"></div>
        ${contentBgFrame}
        <div class="content-item" style='background: ${background}; width: ${width}px; height:${height}px;pointer-events: none;text-align: center;position: relative;overflow: hidden; border-radius: 18px'>
          ${content}
        </div>
        ${dataItem.type == 3 ? handleShowLabel(dataItem.name, width) + dataItem.content: "" }
        `

        let className = dataItem.type == 3?"item-frame": ''
        var node = document.createElement("DIV");
        if (isEdit) {
          className += ` movable movable-${p}`
        } else {
          className += ` notMovable movable-${p}`
        }
        node.className = className;
        node.id = `movable-${totalItem}`
        if(pin){
          node.dataset.pin = 1
        }
        node.dataset.id = dataItem.id
        node.dataset.type = dataItem.type
        node.style = `z-index: ${zIndex};position:${pin? "fixed" : "absolute"}; left:${x}px; top:${y}px; width:${width}px; height:${height}px;`

        if (notInSafe === true) {
          node.className += ` notInArea`
        }
        node.innerHTML = html
        document.getElementById('content').appendChild(node);

      }
    }
  }
  function renderHtmlFrame (dataItem) {
    const style = dataItem.style || window.styleFrameInit;

    return `<div class="bg-frame" style="border-radius: ${style.round + "px"};">
            <div class="bg-image" style="background: ${style.bg}; opacity: ${
              style.opacity
          }; mix-blend-mode: ${style.mixBlend}; z-index: ${
              style.isFront ? "1" : "0"
          };"></div>

          <div class="bg-color" style="opacity: ${style.opacity2}; background: ${
            style.bg2
         }; mix-blend-mode: ${style.mixBlend2}; z-index: ${
            style?.isFront ? "0" : "1"
         };"></div>
          </div>
          `;

  }
  function renderVirtualPrev(){

    
    // const eles = document.getElementsByClassName('movable');
    // for (var i = eles.length - 1; i >= 0; i--) {
    //   eles[i].remove()
    // }
    document.getElementById('content').innerHTML = ''
    document.getElementById('footer').innerHTML = ''
    let html = ''
    for (var p = 0; p < pages.length; p++) {

      if (!isPortrait) {
        html += `<div class="slide" id="slide-${p}" style="width: 100%;height:${windowWidth}px;">`;
      } else {
        html += `<div class="slide" id="slide-${p}" style="width: ${windowWidth}px;height:100%;float:left">`;
      }
      html += '</div>';
    }

 
    for (var i = 0; i < itemInFooters.length; i++) {

      const dataItem = itemInFooters[i];
      if (typeof dataItem !== 'object' || dataItem === null || Object.keys(dataItem).length === 0) {
        continue
      }

      let background = dataItem.backgroundColor;
      let content = '';

      const type = dataItem.type;
      const options = dataItem.options || {};

 
      console.log(dataItem)
      let x = dataItem.position.x * itemWidth + spaceWidth * (dataItem.position.x + 1);
      let y = window.innerHeight - itemWidth - footerBarHeight;


    
      let width = dataItem.position.width * (itemWidth) + (dataItem.position.width - 1) * spaceWidth;
      let height = dataItem.position.height * itemWidth + (dataItem.position.height - 1) * spaceWidth;




      if (!isPortrait) {
        const t = width;
        width = height;
        height = t;

        x = window.innerWidth - itemWidth - footerBarHeight;
        y = dataItem.position.x * itemWidth + spaceWidth * (dataItem.position.x + 1);

        // if (p === 0) {
        //   y = window.innerHeight - y - height;
        // }
      }

      // 
      const totalItem = 1000000 + i + 1;

      mappingFooterValues[totalItem] = {
        x, y, width, height
      };

      if (dataItem.type === 3) {
        window.webkit.messageHandlers.callbackHandler.postMessage(
          JSON.stringify({
            workerId: window.workerId,
            command: 'frame',
            action: 'register',
            content: {
              uuid: totalItem,
              name: dataItem.name,
              x,
              y,
              width,
              height,
              type,
              cornerRadius: options.cornerRadius,
              isAllowBackgroundMode: true,
              url: options.url,
              position: 'fixed',
            }
          })
        );
      }


      // const y = n * itemWidth + spaceWidth;


      let widthItem = Math.floor(width * 0.7) - 2; // pixel border moi canh la 1px
      const heightItem = widthItem
      let contentBgFrame = ''
      const pin = false;
      const spaceItem = Math.floor((width - widthItem) / 4); // pixel border moi canh la 1px
      widthItem = width - spaceItem * 4;
    
      if (dataItem.type == 1 || dataItem.type == 4) {

        let padding = Math.floor(width * 0.1);
        if (padding < 2) {
          padding = 2;
        }
        if (padding % 2 !== 0) {
          padding--;
        }

        content = renderHtmlDapp(dataItem, spaceItem, widthItem, width, fontSize);
      } else if (dataItem.type == 3) {
        contentBgFrame = renderHtmlFrame(dataItem);
        content = `${isEdit ? (pin?closePinSvg:pinSvg) +  editSvg + deleteSvg: ''}`;
      }

      let className = dataItem.type == 3?"item-frame": ''
      if (isEdit) {
        className += ` movable`
      } else {
        className += ` footer`
      }
   
      html += `<div class='${className}' id='movable-${totalItem}' data-id="${dataItem.id}" data-type="${dataItem.type}" style='z-index: 1;position:fixed; left:${x}px; top:${y}px; width:${width}px; height:${height}px;'>
        <div class="warning"></div>
        ${contentBgFrame}
        <div class="content-item"  style='background: ${background}; width: ${width}px; height:${height}px;pointer-events: none;text-align: center;position: relative;overflow: hidden; border-radius: 18px'>
        ${content}
        </div>
        ${dataItem.type == 3 ? handleShowLabel(dataItem.name, width) + dataItem.content: "" }
    </div>`

    }

    document.getElementById('content').innerHTML = html;
    hidePageInFrame(currentPage);
 
    const realPage = currentPage - 1;

    let startPage = realPage - 1;
    if (startPage < 0) {
      startPage = 0;
    }
    hideVirtual(startPage - 1);

    let endPage = realPage + 2;
    if (endPage > pages.length) {
      endPage = pages.length;
    }
    hideVirtual(endPage);

    for (var p = startPage; p < endPage; p++) {

      const dataItems = pages[p];

   
      const page = p + 1;

      if (typeof mappingValues[page] !== 'object' || mappingValues[page] === null) {
        mappingValues[page] = {};
      }
      console.log(mappingValues)

      for (var i = 0; i < dataItems.length; i++) {


        const totalItem = page * 100000 + i + 1;

        let html = '';
 
        const dataItem = dataItems[i];
        if (dataItem === null || typeof dataItem !== 'object') {
          continue;
        }

        let background = dataItem.backgroundColor;
        let content = '';



        const type = dataItem.type;
        const options = dataItem.options || {};


        let x, y, width, height, zIndex = 1, notInSafe = false, pin = false;

        let xNative, yNative;

        let hasMappingValue = false;
        if (typeof mappingValues[page][totalItem] === 'object') {
          pin = mappingValues[page][totalItem].pin;
          hasMappingValue = true;
          x = mappingValues[page][totalItem].x;
          y = mappingValues[page][totalItem].y;
          width = mappingValues[page][totalItem].width;
          height = mappingValues[page][totalItem].height;
          zIndex = mappingValues[page][totalItem].zIndex;
          if (typeof zIndex !== 'number') {
            zIndex = 1;
          }
          notInSafe = mappingValues[page][totalItem].notInSafe;
        } else {

          xNative = dataItem.position.x * itemWidth + spaceWidth * (dataItem.position.x + 1);
          x = p * windowWidth + xNative;
          y = statusBarHeight + dataItem.position.y * itemWidth + spaceWidth * (dataItem.position.y + 1);
          yNative = y;

          width = dataItem.position.width * (itemWidth) + (dataItem.position.width - 1) * spaceWidth;
          height = dataItem.position.height * itemWidth + (dataItem.position.height - 1) * spaceWidth;
      
          if (!isPortrait) {
            const t = width;
            width = height;
            height = t;

            yNative = dataItem.position.x * itemWidth + spaceWidth * (dataItem.position.x + 1);
            y = p * window.innerHeight + yNative;

            x = statusBarHeight + dataItem.position.y * itemWidth + spaceWidth * (dataItem.position.y + 1);
            xNative = x;
            // if (p === 0) {
            //   y = window.innerHeight - y - height;
            // }
          }

      
          mappingValues[page][totalItem] = {
            x, y, width, height, type, options, id: dataItem.id
          }
        }

          
          if (notInSafe !== true && dataItem.type === 3) {
            window.webkit.messageHandlers.callbackHandler.postMessage(
              JSON.stringify({
                workerId: window.workerId,
                command: 'frame',
                action: 'register',
                content: {
                  uuid: totalItem,
                  x: xNative,
                  y: yNative,
                  width,
                  height,
                  page,
                  type,
                  cornerRadius: options.cornerRadius,
                  isAllowBackgroundMode: options.isAllowBackgroundMode,
                  url: options.url,
                }
              })
            );
          }




        // const y = n * itemWidth + spaceWidth;


        let widthItem = Math.floor(width * 0.7) - 2; // pixel border moi canh la 1px
        const heightItem = widthItem

        const spaceItem = Math.floor((width - widthItem) / 4); // pixel border moi canh la 1px
        widthItem = width - spaceItem * 4;

        let contentBgFrame = ''
        if (dataItem.type == 1 || dataItem.type == 4) {

          let padding = Math.floor(width * 0.1);
          if (padding < 2) {
            padding = 2;
          }
          if (padding % 2 !== 0) {
            padding--;
          }
          content = renderHtmlDapp(dataItem, spaceItem, widthItem, width, fontSize)

        } else if (dataItem.type == 3) {
          contentBgFrame = renderHtmlFrame(dataItem);
          content = `${isEdit ? (pin?closePinSvg:pinSvg) +  editSvg + deleteSvg: ''}`;
        }

        html += `<div class="warning"></div>
        ${contentBgFrame}
        <div class="content-item" style='background: ${background}; width: ${width}px; height:${height}px;pointer-events: none;text-align: center;position: relative;overflow: hidden; border-radius: 18px'>
          ${content}
        </div>
        ${dataItem.type == 3 ? handleShowLabel(dataItem.name, width) + dataItem.content: "" }
        `

        let className = dataItem.type == 3?"item-frame": ''
        var node = document.createElement("DIV");
        if (isEdit) {
          className += ` movable movable-${p}`
        } else {
          className += ` notMovable movable-${p}`
        }
        node.className = className;
        node.id = `movable-${totalItem}`
        node.dataset.id = dataItem.id
        node.dataset.type = dataItem.type
        node.style = `z-index: ${zIndex};position:absolute; left:${x}px; top:${y}px; width:${width}px; height:${height}px;`

        if (notInSafe === true) {
          node.className += ` notInArea`
        }
        node.innerHTML = html
        document.getElementById('content').appendChild(node);

      }
    }
  }

  function renderHtmlDapp(dataItem, spaceItem, widthItem, width, fontSize) {
      return `
        <div style="
        padding-left: ${spaceItem * 2}px;
        padding-right: ${spaceItem * 2}px;
        padding-top: ${spaceItem}px;
        padding-bottom: ${spaceItem}px;
    ">
      <div style="
        // border: 1px solid black;
        border-radius: 15px;
        overflow: hidden;
            margin: 0 auto;
        width: ${widthItem - 2}px;
        height: ${widthItem - 2}px;
        background: white;
      ">
        <img src="${dataItem.logo}" style="max-width: 100%;" />
      </div>
      <div style="
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        height: ${width - widthItem - spaceItem * 2}px;
        font-size: ${fontSize}px;
        margin-top: 3px;
      ">
        ${dataItem.name}
      </div>
    </div>
  `;
  }
  function renderProssesBar () {
    if (isPortrait) {
      const eleNavigation = document.getElementById('pageNavigation');
      eleNavigation.offsetWidth
      const node = document.createElement('div')
      node.id = "pageNavigationProgress"
      node.style.left = 5 + 'px';
      node.style.height = '70%';
      node.style.width = Math.floor(currentPage * (eleNavigation.offsetWidth - 10) / pages.length) + 'px';
      eleNavigation.appendChild(node);
    } else {
      document.getElementById('pageNavigationProgress').style.top = 0 + 'px';
      document.getElementById('pageNavigationProgress').style.height = Math.floor(currentPage * window.innerHeight / pages.length) + 'px';
      document.getElementById('pageNavigationProgress').style.width = '100%';
      
    }
  }

  function toggleShowBarController () {
    if(isEdit){
      if(isPortrait){
        const eles = document.querySelectorAll('.btn-have-edit');
        eles.forEach(item => {
          item.style.display = 'grid';
        })
        const eles2 = document.querySelectorAll('.icon-navigation');
        eles2.forEach(item => {
          item.style.display = 'none';
        })
        document.getElementById('pageNavigation').style.left = (window.innerWidth / 2 - 78 / 2 - 28) + "px";
      }else {
        const eles = document.querySelectorAll('.btn-have-edit');
        eles.forEach(item => {
          item.style.display = 'grid';
          item.classList.add('noPortrait')
        })
        const eles2 = document.querySelectorAll('.icon-navigation');
        eles2.forEach(item => {
          item.style.display = 'none';
        })
        document.getElementById('pageNavigation').style.left = "unset"
      }
      // document.getElementById('footer').style.height = "170px";
    }else{
      if(isPortrait){
        const eles = document.querySelectorAll('.btn-have-edit');
        eles.forEach(item => {
          item.style.display = 'none';
        })
    
        const eles2 = document.querySelectorAll('.icon-navigation');
        eles2.forEach(item => {
          item.style.display = 'flex';
          if(item.classList.contains('noPortrait')){
            item.classList.add('noPortrait')
          } 
        })
        document.getElementById('pageNavigation').style.left = (window.innerWidth / 2 - itemWidth / 2) + "px";
      }else {
        const eles = document.querySelectorAll('.btn-have-edit');
        eles.forEach(item => {
          item.style.display = 'none';
        })
    
        const eles2 = document.querySelectorAll('.icon-navigation');
        eles2.forEach(item => {
          item.style.display = 'flex';
          item.classList.add('noPortrait')
        })
        document.getElementById('pageNavigation').style.left = "unset";
      }
  
    }
  }

  // phan keo tha

  let isMove = 0;

  var moveAddPosition;

  function checkAddItem (moveAddPosition) {
    console.log(numberColumn,numberRow,  moveAddPosition)
    if(moveAddPosition.width > numberColumn - moveAddPosition.x){
      moveAddPosition.width = numberColumn - moveAddPosition.x
    }
    if(moveAddPosition.height > numberRow - moveAddPosition.y){
      moveAddPosition.height = numberRow - moveAddPosition.y
    }
    if(moveAddPosition.height == 1 && moveAddPosition.width == 1){
      Send({command: 'show-add-d-app', value : {...moveAddPosition, page: currentPage}})
    }else {
      Send({command: 'show-add-frame', value : {...moveAddPosition, page: currentPage}})
      // const arrX = []
      // for(let i = moveAddPosition.x; i < moveAddPosition.x + moveAddPosition.width; i++) {
      //   arrX.push(i * itemWidth +  (i + 1) * spaceWidth)
      // }

      // const arrY = []
      // for(let i = moveAddPosition.y; i < moveAddPosition.y + moveAddPosition.width; i++) {
      //   arrY.push((i * itemWidth +  (i + 1) * spaceWidth) + statusBarHeight)
      // }
      // let matchPosition = false;

      // let xNative = moveAddPosition.x * itemWidth + spaceWidth * (moveAddPosition.x + 1);
      // const x = (currentPage - 1) * windowWidth + xNative;

      // const y = statusBarHeight + moveAddPosition.y * itemWidth + spaceWidth * (moveAddPosition.y + 1);
 
      // const width = moveAddPosition.width * (itemWidth) + (moveAddPosition.width - 1) * spaceWidth;
      // const height = moveAddPosition.height * itemWidth + (moveAddPosition.height - 1) * spaceWidth;
      // for (var n in mappingValues[currentPage]) {
      //   // if (n == id) {
      //   //   continue;
      //   // }

      //   if (typeof mappingValues[currentPage][n] !== 'object' || mappingValues[currentPage][n] === null || mappingValues[currentPage][n].notInSafe === true) {
      //     continue;
      //   }


      //   if (
      //     x < mappingValues[currentPage][n].x &&
      //     x + width <= mappingValues[currentPage][n].x
      //   ) {
      //     continue;
      //   }
      //   if (
      //     x >= mappingValues[currentPage][n].x + mappingValues[currentPage][n].width
      //   ) {
      //     continue;
      //   }


      //   if (
      //     y < mappingValues[currentPage][n].y &&
      //     y + height <= mappingValues[currentPage][n].y
      //   ) {
      //     continue;
      //   }
      //   if (
      //     y >= mappingValues[currentPage][n].y + mappingValues[currentPage][n].height
      //   ) {
      //     continue;
      //   }
      //   matchPosition = true;
      //   break;

      // }
      
      // if(matchPosition){
      //   alert("trung")
      //   const dataItem = {
      //     id: "23ioii23",
      //     name: "youtube",
      //     content: "",
      //     type: 3,
      //     options: {
      //       url: "https://www.youtube.com/embed/56xBfYg5neE",
      //     },
      //     position: {
      //       x: moveAddPosition.x,
      //       y: moveAddPosition.y,
      //       width: moveAddPosition.width,
      //       height: moveAddPosition.height,
      //     },
      //   }
        
      //   let dataCurrentPage = pages[currentPage - 1];
      //   dataCurrentPage.push(dataItem)
      //   pages[currentPage - 1] = dataCurrentPage;
      //   setPages()
      //   // const xNative = dataItem.position.x * itemWidth + spaceWidth * (dataItem.position.x + 1);
      //   // const x = (currentPage - 1) * windowWidth + xNative;

      //   // const y = statusBarHeight + dataItem.position.y * itemWidth + spaceWidth * (dataItem.position.y + 1);
   
      //   // const width = dataItem.position.width * (itemWidth) + (dataItem.position.width - 1) * spaceWidth;
      //   // const height = dataItem.position.height * itemWidth + (dataItem.position.height - 1) * spaceWidth;
      
      // mappingValues[currentPage][currentPage + '0000' + (dataCurrentPage.length)] = {
      //   x, y, width,height, 
      //   options: dataItem.options,
      //   type: dataItem.type,
      //   notInSafe: true,
      // }
      // setMappingValues()
      // renderVirtualPrev();
      // renderGridMain();
      // renderGridFooter();

      // }else {
      //   const dataItem = {
      //     id: "23ioii23",
      //     name: "youtube",
      //     content: "",
      //     type: 3,
      //     options: {
      //       url: "https://www.youtube.com/embed/56xBfYg5neE",
      //     },
      //     position: {
      //       x: moveAddPosition.x,
      //       y: moveAddPosition.y,
      //       width: moveAddPosition.width,
      //       height: moveAddPosition.height,
      //     },
      //   }

      //   let dataCurrentPage = pages[currentPage - 1];
      //   dataCurrentPage.push(dataItem)
      //   pages[currentPage - 1] = dataCurrentPage;
      //   setPages()
      //     // xNative = dataItem.position.x * itemWidth + spaceWidth * (dataItem.position.x + 1);
      //     // const x = (currentPage - 1) * windowWidth + xNative;

      //     // const y = statusBarHeight + dataItem.position.y * itemWidth + spaceWidth * (dataItem.position.y + 1);
     
      //     // const width = dataItem.position.width * (itemWidth) + (dataItem.position.width - 1) * spaceWidth;
      //     // const height = dataItem.position.height * itemWidth + (dataItem.position.height - 1) * spaceWidth;
        
      //   mappingValues[currentPage][currentPage + '0000' + (dataCurrentPage.length)] = {
      //     x, y, width,height, 
      //     options: dataItem.options,
      //     type: dataItem.type,
      //   }
      //   setMappingValues()
      //   renderVirtualPrev();
      //   renderGridMain();
      //   renderGridFooter();
      // }
      // console.log("x", moveAddPosition.x * itemWidth +  (moveAddPosition.x + 1) * spaceWidth)
      // console.log("y", (moveAddPosition.y * itemWidth +  (moveAddPosition.y + 1) * spaceWidth) + statusBarHeight)
      
    }
  }

  function processMouseUp(ele, countProcess = 0) {
    if(handleClickTargetOther(targetIconFrame)) return;
  
    clearTimeout(timeoutChangeSlide);
    document.getElementById('pageNavigationProgress')?.remove();
    if (isMoveAdd === true || isMovePageNavigation === true) {

      if (isMoveAdd === true) {
        isMoveAdd = false;

        const eleParent = document.getElementById(`slide-${currentPage - 1}`);
        for (var i = 0; i < eleParent.childNodes.length; i++) {

          eleParent.childNodes[i].style.background = '';
        }

        if (typeof moveAddPosition === 'object' && moveAddPosition !== null && typeof moveAddPosition.x === 'number') {
          checkAddItem(moveAddPosition)
        }
        moveAddPosition = false;

      } else {

        // active lai
        hidePageInFrame(currentPage - 2);
        hidePageInFrame(currentPage + 2);
        activePageInFrame(currentPage);

        isLockChangePage = false

        isMovePageNavigation = false;
        renderNumberNavigation()
        moveAddPosition = false;
      }

      return;
    }
    console.log('=======click=2==', isMove, isMoveMain)


    if (isMove === 1) {
      console.log('=======click=1==')
      isMove = 0;

      let id = ele.id.replace("movable-", "") * 1;
      let isFooter = false;

      if (id.length === 7) {
        id -= 10000000 - 1;
        isFooter = true;
      } else {
        id -= currentPage * 1000000 - 1;
      }

      // if (isEdit) {
      //   alert('can not open in edit mode');
      //   return;
      // }
      console.log('--------click: ', id, isFooter);
      return
    }
    if (isMoveMain) {

      // neu dang di chuyen main  va tha o doi tuong  mai

      isMoveMain = false;

      if (isLockChangePage) {
        return;
      }
      isLockChangePage = true;
      const clientX = lastMainClientX;
      const clientY = lastMainClientY;

      let spaceCompare = 0;
      let isMoveRight = false;
      if (isPortrait) {
        if (mainPos3 < clientX) {
          spaceCompare = clientX - mainPos3;
          isMoveRight = true;
        } else {
          spaceCompare = mainPos3 - clientX;
        }
      } else {
        if (mainPos4 < clientY) {
          spaceCompare = clientY - mainPos4;
          isMoveRight = true;
        } else {
          spaceCompare = mainPos4 - clientY;
        }
      }


      console.log('-------mainPos3: ', mainPos3, ', clientX', clientX, lastMainClientX, 'lastClientX', lastClientX, pos3);
      const timeCompare = performance.now() - timeMoveX;
      console.log('-------processMouseUp-', isMoveRight, spaceCompare, timeCompare, currentPage);

      let isBack;


      if (
        spaceCompare === 0 &&
        timeCompare < 300
      ) {
        // kiem tra co doi tuong target
        var hasClass = ele.classList.contains('movable-' + (currentPage - 1));
        if (hasClass) {
        
          isLockChangePage = false;
          const id = ele.id.replace("movable-", "") * 1 - currentPage * 100000 - 1;

        
          //  kiem tra dang show modal thi dong
          if (document.getElementById("modal-touch")) {
            closeModal(ele)
            return;
          }

          console.log("koko")
          // alert(`click----page: ${currentPage}--${id}, name: ${pages[currentPage - 1][id].name} in main`)
          Send({command: 'open-dapp', value: pages[currentPage - 1][id]})
          return;
        } else {
          var hasClass = ele.classList.contains('footer');
          if (hasClass) {
            isLockChangePage = false;
            const id = ele.id.replace("movable-", "") * 1 - 1000000 - 1;
            Send({command: 'open-dapp', value: itemInFooters[id]})
            return;
          }

        }
      }
      if (
        spaceCompare > 50 &&
        spaceCompare < 500 &&
        (timeCompare < 300)
      ) {
        console.log('------fast');
        if (!isMoveRight && currentPage < pages.length) {
          isBack = false;
        } else if (isMoveRight && currentPage > 1) {
          isBack = true;
        }
        // return;
      } else {

        if (!isMoveRight && currentPage < pages.length && spaceCompare > windowWidth / 2) {
          isBack = false;
        } else if (isMoveRight && currentPage > 1 && spaceCompare > windowWidth / 2) {
          isBack = true;
        }
      }

      if (typeof isBack === 'undefined') {
        console.log('exi-----------', _target)

        // moveFrame(0, 0);

        // isLockChangePage = false;

        if (isMove !== 0) {
          _target.style.left = lastLeft + "px";
          _target.style.top = lastTop + "px";
        }
        scrollTo(((currentPage - 1) * windowWidth), function () {
          isLockChangePage = false
        }, 300);
        return;
      }
      console.log('isBack', isBack)



      // neu dang di chuyen item thi cong them vao
      // if (isMove !== 0) {
      //   // calculate the new cursor position:
      //   pos1 = pos3 - clientX;
      //   pos2 = pos4 - clientY;
      //   pos3 = clientX;
      //   pos4 = clientY;
      // }

      const top = lastTop;
      const left = lastLeft;

      console.log('=========isMove: ', isMove, left, top);
      const lastPage = currentPage;

      if (isBack) {

        // neu dang di chuyen item thi cong them vao
        if (isMove !== 0) {
          if (isPortrait) {
            lastLeft = (left - window.innerWidth);
            _target.style.left = lastLeft + "px";
          } else {
            lastTop = (top - window.innerHeight)
            _target.style.top = lastTop + "px";
          }
        }

        currentPage--;

        // Swipes to the next slide

      } else {

        // neu dang di chuyen item thi cong them vao
        if (isMove !== 0) {
          if (isPortrait) {
            lastLeft = (left + window.innerWidth);
            _target.style.left = lastLeft + "px";
          } else {
            lastTop = (top + window.innerHeight)
            _target.style.top = lastTop + "px";
          }
        }

        currentPage++;
      }

      if (currentPage > pages.length) {
        currentPage = pages.length;
      } else if (currentPage < 1) {
        currentPage = 1;
      }

      if (lastPage === currentPage) {
        isLockChangePage = false
        return;
      }

      setPageNumber();

      // doi  vi tri page cho doi tuong
      if (_target !== null && typeof _target === 'object') {
        const id = _target.id.replace("movable-", "") * 1;


        mappingValues[currentPage][id] = { ...mappingValues[lastPage][id] }
        mappingValues[lastPage][id] = null;
        delete mappingValues[lastPage][id];

        setMappingValues();


        console.log('id', id)
        changeClassName(_target, id);

      }
      renderVirtual()

      scrollTo(((currentPage - 1) * windowWidth), function () {
        isLockChangePage = false
        activePageInFrame(currentPage);
      }, 300);
      // document.getElementById('main').scroll({
      //   left: ((currentPage - 1) * windowWidth),
      //   behavior: 'smooth'
      // });


      return
    }

    isMove = 0;

    if (!ele || typeof ele.id !== 'string') {
      return;
    }
    const id = ele.id.replace("movable-", "") * 1;

    const mappingNewValue = {};



    mappingNewValue.y = ele.offsetTop - pos2;
    mappingNewValue.x = ele.offsetLeft - pos1;


    // const basicX = (currentPage - 1) * windowWidth;
    // const basicY = mappingNewValue.y;

    let basicX;
    let basicY;

    if (isPortrait) {
      basicX = mappingNewValue.x - (currentPage - 1) * window.innerWidth;
      basicY = mappingNewValue.y;
    } else {
      basicX = mappingNewValue.x;
      basicY = mappingNewValue.y - (currentPage - 1) * window.innerHeight;
    }

    // tinh vi tri khung luoi, de so x va y gan nhat
    let isExists = false;
    for (var n = 0; n < mappingDefaultValues.length; n++) {

      if (
        basicX < mappingDefaultValues[n].x - 20 ||
        basicX > mappingDefaultValues[n].x + 20
      ) {
        continue;
      }
      if (
        basicY < mappingDefaultValues[n].y - 20 ||
        basicY > mappingDefaultValues[n].y + 20
      ) {
        continue;
      }

      isExists = n;
      break;
    }

    // nếu là frame dc pin
    if(ele.dataset.pin == 1){
      const p = getPage(id)
      if(ele.dataset.footer == 1){
        mappingFooterValues[id].x = ele.offsetLeft
        mappingFooterValues[id].y = ele.offsetTop
        setMappingFooterValues()
      }else{
        mappingValues[p][id].x = ele.offsetLeft
        mappingValues[p][id].y = ele.offsetTop
        setMappingValues()
      }
      return;
    }

    // kiem tra co loi vao vung footer
    if (isExists === false) {

      const mappingTmp = {};
      let isInScreen = false;
      if (isPortrait) {

        const compareHeight = window.innerHeight - basicY;



        if (basicX > -10 && basicX < window.innerWidth + 10 && compareHeight > 10 && compareHeight - 20 < itemWidth) {
          isInScreen = true;
        }

        console.log('basicX', basicX, compareHeight, isInScreen);

        // so chieu ngang va chieu cao co thoa
        if (isInScreen) {

          if (basicX + ele.clientWidth - 10 > window.innerWidth) {
            isInScreen = false;
          }
        }
        console.log('basicX', basicX, compareHeight, isInScreen);

        if (isInScreen) {

          if (ele.clientHeight > itemWidth) {
            isInScreen = false;
          }
        }
        console.log('basicX', basicX, compareHeight, isInScreen);
      } else {

        const compareWidth = window.innerWidth - basicX;
        console.log('basicY', basicY, compareWidth, isInScreen);

        if (basicY > -10 && basicY < window.innerHeight + 10 && compareWidth > 10 && compareWidth - 20 < itemWidth) {
          isInScreen = true;
        }
        console.log('basicY', basicY, compareWidth, isInScreen, basicY < window.innerHeight + 10, compareWidth - 10 < itemWidth, itemWidth);

        // so chieu ngang va chieu cao co thoa
        if (isInScreen) {

          if (basicY + ele.clientHeight - 10 > window.innerHeight) {
            isInScreen = false;
          }
        }
        console.log('basicY', basicY, compareWidth, isInScreen);

        if (isInScreen) {

          if (ele.clientWidth > itemWidth) {
            isInScreen = false;
          }
        }

      }

      // kiem tra co dang de len item duoi
      if (isInScreen !== false) {

        if (isPortrait) {

          // tinh lai vi tri theo chuan cua footer
          const pos = Math.round(basicX / (itemWidth + spaceWidth))
          const x = pos * itemWidth + (pos + 1) * spaceWidth;
          let y = window.innerHeight - itemWidth - footerBarHeight;

          mappingTmp.x = x;
          mappingTmp.y = y
        } else {

         
          // tinh lai vi tri theo chuan cua footer
          const pos = Math.round(basicY / (itemWidth + spaceWidth))
          const x = window.innerWidth - itemWidth - footerBarHeight;
          let y = pos * itemWidth + (pos + 1) * spaceWidth;


          mappingTmp.x = x;
          mappingTmp.y = y;
        }

        if (ele.style.position === 'fixed') {
          mappingTmp.height = mappingFooterValues[id].height
          mappingTmp.width = mappingFooterValues[id].width
        } else {

          mappingTmp.height = mappingValues[currentPage][id].height
          mappingTmp.width = mappingValues[currentPage][id].width

        }
      }
      console.log('basicX', mappingTmp, isInScreen);


      if (typeof mappingTmp.x !== 'undefined') {
        for (var n in mappingFooterValues) {

          if (n == id) {
            continue;
          }

          if (typeof mappingFooterValues[n] !== 'object' || mappingFooterValues[n] === null || mappingFooterValues[n].notInSafe === true) {
            continue;
          }


          if (
            mappingTmp.x < mappingFooterValues[n].x &&
            mappingTmp.x + mappingTmp.width <= mappingFooterValues[n].x
          ) {
            continue;
          }
          if (
            mappingTmp.x >= mappingFooterValues[n].x + mappingFooterValues[n].width
          ) {
            continue;
          }


          if (
            mappingTmp.y < mappingFooterValues[n].y &&
            mappingTmp.y + mappingTmp.height <= mappingFooterValues[n].y
          ) {
            continue;
          }
          if (
            mappingTmp.y >= mappingFooterValues[n].y + mappingFooterValues[n].height
          ) {
            continue;
          }


          isInScreen = false;
          break;

        }
      }
      if (isInScreen) {

        // if (typeof mappingFooterValues[id] !== 'object' || mappingFooterValues[id] === null) {
        const x = mappingTmp.x;
        const y = mappingTmp.y;

        let lastPage = -1;
        ele.classList.forEach(function (className) {
          if (className.indexOf("movable-") === -1) {
            return;
          }
          ele.classList.remove(className);
          lastPage = className.replace("movable-", '') * 1;
        })


        if (lastPage > -1) {

          console.log('--------change-itemInFooters', id, x, y, JSON.stringify(itemInFooters));
          const newId = 1000000 + itemInFooters.length + 1;
          // ele.classList.remove("movable-" + (currentPage - 1));


          if (typeof mappingValues[currentPage][id] === 'object' && mappingValues[currentPage][id] !== null) {

            mappingFooterValues[newId] = { ...mappingValues[currentPage][id], x, y, notInSafe: null };

            mappingValues[currentPage][id] = null;
            delete mappingValues[currentPage][id];

            setMappingValues();
            setMappingFooterValues();

          }
          ele.id = 'movable-' + (newId);


          const indexInPage = id - currentPage * 100000 - 1
          itemInFooters.push({
            ...pages[currentPage - 1][indexInPage],
            position: {
              ...pages[currentPage - 1][indexInPage].position,
              x: Math.floor(x / itemWidth),
              y: 0
            }
          });
          setItemInFooters();

          // ele.classList.add("movable-" + (currentPage - 1));


          // }

        } else {
          // sua lai footer
          mappingFooterValues[id].x = x
          mappingFooterValues[id].y = y
          mappingFooterValues[id].notInSafe = null;
          setMappingFooterValues();
        }
  
        ele.style.position = 'fixed';
        ele.style.top = y + "px";
        ele.style.left = x + "px";

        ele.classList.remove("notInArea");



        if (typeof customItemInFooters[id] === 'object') {
          // alert('-----err: ab2301051708')

          itemInFooters.push({
            ...customItemInFooters[id]
          });
          setItemInFooters();
          customItemInFooters[id] = null;

          setCustomItemInFooters();
          return;
        }


        return;
      }


    }

    console.log('-------isExists-mappingDefaultValues', isExists)
    if (isExists === false) {
      let isRollback = false;

      if (isPortrait) {

        if (basicX < 10) {
          isRollback = true;
        } else if (basicX + ele.clientWidth + 10 >= window.innerWidth) {
          isRollback = true;
        } else if (basicY > window.allowDragHeight + ele.clientHeight * 0.5 || basicY < ele.clientHeight * -0.5) {
          isRollback = true;
        }

      } else {

        console.log('========', basicY, ele.clientHeight)

        if (basicY < 10) {
          isRollback = true;
        } else if (basicY + ele.clientHeight + 10 >= window.innerHeight) {
          isRollback = true;
        } else if (basicX > window.allowDragWidth + ele.clientWidth * 0.5 || basicX < ele.clientWidth * -0.5) {
          isRollback = true;
        }

      }

      console.log('====isRollback====', isRollback)
      if (isRollback) {

        // rollback thi ko cho chuyen page
        clearTimeout(timeoutChangeSlide);


        if (mouseDownClientX === false || 1 === 1) {
          // neu dang move 1 doi tuong ma ko co vi tri co dinh, thi ko snap ve lai

          let top = window.allowDragHeight / 2;
          let left = window.innerWidth * (currentPage - 1) + window.innerWidth / 2;
          if (!isPortrait) {
            left = window.allowDragHeight / 2 + ele.clientWidth / 2;
            top = window.innerHeight * (currentPage - 1) + window.innerHeight / 2 - ele.clientHeight / 2;
          }


          console.log('left', left, ele.style.left, basicX)
          ele.style.top = top + "px";
          ele.style.left = left + "px";
        } else {

          // // const left = (_target.offsetLeft - pos1);
          // console.log('isRollback', isRollback, pos3, pos4)

          const top = Math.floor(mouseDownClientY / itemWidth) * itemWidth;
          const left = Math.floor(mouseDownClientX / itemWidth) * itemWidth;
          // // console.log('mouseDownClientX', mouseDownClientX, ele.clientWidth, ele.offsetLeft)
          // // set the element's new position:
          ele.style.top = top + "px";
          ele.style.left = left + "px";
          // // tinh lai vi tri
          if (countProcess < 1) {
            console.log('------process');
            processMouseUp(ele, 1);

          }
          // return;
        }

        isExists = false;
      }

    }

    // check co dang nam ngoai pham vi cho phep
    if (isExists !== false) {
      // if (left < 0 || left + e.target.clientWidth > windowWidth) {
      //   return;
      // }
      // if (top < 0 || top + e.target.clientHeight > windowHeight) {
      //   return;
      // }


      let width
      let height

      if (ele.style.position === 'fixed') {
        width = mappingFooterValues[id].width
        height = mappingFooterValues[id].height
      } else {

        console.log('-------mappingValues[currentPage]', id, mappingValues[currentPage])



        width = mappingValues[currentPage][id].width
        height = mappingValues[currentPage][id].height
      }


      console.log('-------window.allowDragHeight-1', window.allowDragHeight, basicY, height)
      if (isPortrait) {

        if (basicY > window.allowDragHeight || basicY + height > window.allowDragHeight) {
          console.log('-------isExists-1', isExists, basicY, height, window.allowDragHeight, basicY > window.allowDragHeight, basicY + height > window.allowDragHeight)
          isExists = false;
        }

        if (basicX > windowWidth || basicX + width > windowWidth) {
          console.log('-------isExists-2', isExists, basicX, width, windowWidth * (currentPage - 1))

          isExists = false;
        }
      } else {
        if (basicX > window.allowDragWidth || basicX + width > window.allowDragWidth) {
          console.log('-------isExists-3', isExists, window.allowDragWidth)
          isExists = false;
        }

        if (basicY > windowHeight || basicY + height > windowHeight) {
          console.log('-------isExists-4', isExists, basicY, height, windowHeight * (currentPage - 1))

          isExists = false;
        }
      }
      console.log('-------isExists-5', isExists)
    }


    const mappingTmp = {};



    if (isExists !== false) {

      if (isPortrait) {
        mappingTmp.x = mappingDefaultValues[isExists].x + (currentPage - 1) * window.innerWidth;
        mappingTmp.y = mappingDefaultValues[isExists].y
      } else {
        mappingTmp.x = mappingDefaultValues[isExists].x;
        mappingTmp.y = mappingDefaultValues[isExists].y + (currentPage - 1) * window.innerHeight;
      }

      if (ele.style.position === 'fixed') {
        mappingTmp.width = mappingFooterValues[id].width
        mappingTmp.height = mappingFooterValues[id].height
      } else {
        mappingTmp.width = mappingValues[currentPage][id].width
        mappingTmp.height = mappingValues[currentPage][id].height
      }

      // cho rollback ve lai vi tri cu neu di chuyen chi trong cung 1 o
      // tinh vi tri o dang loi, neu trung isExists thi xet lai gia tri ve nhu cu
      const n = id;
      if (
        !isForceRenderVirtual &&
        typeof mappingValues[currentPage][n] === 'object' &&
        mappingValues[currentPage][n] !== null &&
        mappingTmp.x === mappingValues[currentPage][n].x &&
        mappingTmp.y === mappingValues[currentPage][n].y
      ) {
        mappingValues[currentPage][id].x = mappingTmp.x
        mappingValues[currentPage][id].y = mappingTmp.y

        setMappingValues();


        ele.style.top = mappingTmp.y + "px";
        ele.style.left = mappingTmp.x + "px";

        ele.style.zIndex = zIndex - 1;

        ele.classList.remove("notInArea");
        ele = null;
        return;

      }
    }
console.log(mappingTmp)
    if (isExists !== false) {
      for (var n in mappingValues[currentPage]) {
        if (n == id) {
          continue;
        }

        if (typeof mappingValues[currentPage][n] !== 'object' || mappingValues[currentPage][n] === null || mappingValues[currentPage][n].notInSafe === true) {
          continue;
        }


        if (
          mappingTmp.x < mappingValues[currentPage][n].x &&
          mappingTmp.x + mappingTmp.width <= mappingValues[currentPage][n].x
        ) {
          continue;
        }
        if (
          mappingTmp.x >= mappingValues[currentPage][n].x + mappingValues[currentPage][n].width
        ) {
          continue;
        }


        if (
          mappingTmp.y < mappingValues[currentPage][n].y &&
          mappingTmp.y + mappingTmp.height <= mappingValues[currentPage][n].y
        ) {
          continue;
        }
        if (
          mappingTmp.y >= mappingValues[currentPage][n].y + mappingValues[currentPage][n].height
        ) {
          continue;
        }


        isExists = false;
        break;

      }
      
      console.log('-------isExists', isExists)

    }



    // neu loi tu footer vao
    if (ele.style.position === 'fixed') {
      mappingValues[currentPage][id] = { ...mappingFooterValues[id] };

      mappingFooterValues[id] = null;
      delete mappingFooterValues[id];

      setMappingValues();
      setMappingFooterValues();
    }

    if (isExists !== false) {


      console.log('mappingTmp.x', mappingTmp.x, mappingTmp.y)
      mappingValues[currentPage][id].x = mappingTmp.x
      mappingValues[currentPage][id].y = mappingTmp.y

      ele.style.top = mappingTmp.y + "px";
      ele.style.left = mappingTmp.x + "px";

      ele.style.zIndex = zIndex - 1;

      mappingValues[currentPage][id].notInSafe = false;
      mappingValues[currentPage][id].zIndex = zIndex;

      setMappingValues();

      ele.classList.remove("notInArea");

      // neu ton tai trong customItem thi them den pages

      if (typeof customItemInFooters[id] === 'object' && customItemInFooters[id] !== null) {

        console.log('===============customItemInFooters', id, pages.length, customItemInFooters[id]);


        // alert('-----err: ab2301051708')
        pages[currentPage - 1].push({
          ...customItemInFooters[id],
          position: {
            ...customItemInFooters[id].position,
            x: mappingDefaultValues[isExists].col,
            y: mappingDefaultValues[isExists].row,
          }
        })
        setPages();

        customItemInFooters[id] = null;

        setCustomItemInFooters();
        return;
      }
    } else {


      console.log('mappingNewValue.x', mappingNewValue.x, mappingNewValue.y, 'currentPage', currentPage, 'id', id)
      mappingValues[currentPage][id].x = mappingNewValue.x
      mappingValues[currentPage][id].y = mappingNewValue.y

      ele.style.zIndex = zIndexNotInArea;

      mappingValues[currentPage][id].notInSafe = true;
      mappingValues[currentPage][id].zIndex = zIndexNotInArea;

      setMappingValues();

      ele.classList.add("notInArea");
    }
    changeClassName(ele, id);
    ele.style.position = "absolute";

  }

  function changeClassName(ele, id) {

    // neu ko doi page
    var hasClass = ele.classList.contains("movable-" + (currentPage - 1));

    if (hasClass) {
      return;
    }


    // doi ten class
    let lastPage = -1;
    ele.classList.forEach(function (className) {
      if (className.indexOf("movable-") === -1) {
        return;
      }
      ele.classList.remove(className);
      lastPage = className.replace("movable-", '') * 1;
    })

    ele.classList.add("movable-" + (currentPage - 1));

    // // const totalItem = page * 100000 + i + 1;
    // let id = ele.id.replace("movable-", '') * 1;
    let indexInPage;
    if (lastPage < 0) {
      indexInPage = id - 1000000;
    } else {
      indexInPage = id - (lastPage + 1) * 100000;
    }
    indexInPage -= 1;


    const { x, y } = mappingValues[currentPage][id];

    let row, col;
    // if (isPortrait) {
    row = Math.floor(x / (itemWidth + spaceWidth));
    col = Math.floor(y / (itemWidth + spaceWidth));
    // }
    console.log('---lastPage----', lastPage, 'id', id, 'indexInPage', indexInPage, 'x', x, 'y', y, currentPage, 'itemInFooters', JSON.stringify(itemInFooters))
    // them data
    // lay tu footer
    if (lastPage < 0) {
      pages[currentPage - 1].push({
        ...itemInFooters[indexInPage],
        position: {
          ...itemInFooters[indexInPage].position,
          // add data x, y moi
          x: row, y: col
        }
      });
    } else {
      pages[currentPage - 1].push({
        ...pages[lastPage][indexInPage],
        position: {
          ...pages[lastPage][indexInPage].position,
          // add data x:row, y mo: col
          x: row, y: col
        }
      });
    }
    setPages();

    let newId;

    if (lastPage < 0) {


      // neu dang ko trong vong cho phep
      if (mappingValues[currentPage][id].notInSafe === true) {
        newId = (currentPage * 100000 + pages[currentPage - 1].length);

        mappingValues[currentPage][newId] = { ...mappingValues[currentPage][id] };

        setMappingValues();


        console.log('---customItemInFooters----save-------', newId)
        customItemInFooters[newId] = { ...itemInFooters[indexInPage] }

        itemInFooters[indexInPage] = null;
        setItemInFooters();

        setCustomItemInFooters();

        console.log('-itemInFooters[indexInPage]-----', id, indexInPage, newId, mappingValues[currentPage][id]);
      } else {

        // console.log('---customItemInFooters----id-------', id)
        // kiem tra dang chuyen tu foooter ko
        // if (id.length === 7) {
        //   id -= 10000000 - 1;
        //   isFooter = true;
        // } else {
        //   id -= currentPage  * 1000000 - 1;
        // }

        newId = (currentPage * 100000 + pages[currentPage - 1].length);
        mappingValues[currentPage][newId] = { ...mappingValues[currentPage][id] };

        mappingValues[currentPage][id] = null;
        console.log('-----itemInFooters.length-', id, itemInFooters, itemInFooters.length, newId);

        setMappingValues();
      }

    } else {

      newId = (currentPage * 100000 + pages[currentPage - 1].length);
      pages[lastPage][indexInPage] = null;

      mappingValues[currentPage][newId] = { ...mappingValues[currentPage][id] };

      mappingValues[currentPage][id] = null;
      delete mappingValues[currentPage][id];

      setMappingValues();

      console.log('------3');
    }
    console.log('------4');

    ele.id = "movable-" + newId;

    // change map



  }
  function handleEventUp(ele) {
    timeOutMuti && clearTimeout(timeOutMuti);
    timeOutShadow && clearTimeout(timeOutShadow);

    if(targetShadow && nodeTargetShadow){
      nodeTargetShadow.remove()
    }
    console.log('-----');
    if (
      (isMove === 0 && isMoveMain) ||
      (isMove !== 0 && !isMoveMain)
    ) {
      window.removeEventListener('mousemove', handleEventMove, true);
      window.removeEventListener('touchmove', handleEventMove, true);
    }
    if (isMove !== 0 && (_target === null || typeof _target !== 'object')) {
      return;
    }
    if (isMoveMain) {
      processMouseUp(ele.target);
    } else {
      processMouseUp(_target);
    }

    // sau khi xu ly xong ma isMove duoc dua ve mac dinh thi remove target
    if (isMove === 0) {
      _target = null;
    }

    // const eles = document.getElementsByClassName("notInArea");
    // for(var i = 0; i < eles.length; i++) {
    //   processMouseUp(eles[i]); 
    // }

  }

  let pageInEventDown = -1;
  let isMoveAdd = false;
  var isMovePageNavigation = false;
  let isMoveMain = false, mouseDownClientX, mouseDownClientY;

  var getPage = (value) => {
    const v =  value.toString().split('');
    let index0 = 0;
    for(let i = 0; i < v.length - 1; i++) {
      if(v[i] == 0){
        index0 = i;
      }
    } 
    return v.slice(0, index0 + 1 - 4).join('')
  }
  var handleClickTargetOther = (value) => {
    if(!value) return;
    const p = getPage(value.idDefault);
    targetIconFrame = null;
    let data = null; 
    if(value.id == "icon-edit-frame"){
      for(let i = 0; i < pages.length; i++){
        for(let j = 0; j < pages[i].length; j++){
          if(pages[i][j].id == value.idFrame){
            data = pages[i][j];
            break;
          }
        }
        if(data){
          break;
        }
      }
      Send({command: "show-edit-frame", value: data})
      return true;
    }
    if(value.id == "icon-pin-frame"){
      const ele = document.querySelectorAll(`[data-id="${value.idFrame}"]`)[0];
      if(ele.dataset.pin != 1){
        ele.style.position = "fixed";
        ele.style.left = ele.offsetLeft % window.innerWidth + "px"
        ele.style.zIndex = 50;
        ele.dataset.pin = 1;
  
        for( let i in mappingValues[p]){
         
            if(mappingValues[p][i].id == value.idFrame){
              mappingValues[p][i].pin = true;
              mappingValues[p][i].zIndex = 50;
            }
        }
        isMoveMain = false;
        setMappingValues(mappingValues)
      }else {
        
        ele.style.position = "absolute";
        ele.style.left = ele.offsetLeft + window.innerWidth * (currentPage - 1) + "px";
        ele.style.zIndex = 1;
        ele.dataset.pin = 0;
  
        for( let i in mappingValues[p]){
         
            if(mappingValues[p][i].id == value.idFrame){
              mappingValues[p][i].pin = false;
              mappingValues[p][i].zIndex = 1;
              if(p != currentPage){
                mappingValues[currentPage][currentPage + '0000' + (Object.keys(mappingValues[currentPage]).length + 1)] = {
                  ...mappingValues[p][i], pin: true, zIndex: 1
                }
                mappingValues[p][i] = null;
                delete mappingValues[p][i]
              }
             
              break;
         
            }
        }
        let datap = [];
        let dataDelete = []
        console.log(p , currentPage)
        if(p != currentPage){
          for( let i in pages[p - 1]){
              if(pages[p-1][i].id !== value.idFrame){
                datap.push(pages[p-1][i])
              }else {
                dataDelete.push(pages[p-1][i])
              }
          }
          pages[p - 1] = datap;
          pages[currentPage - 1] = [...pages[currentPage - 1], ...dataDelete];

        }
      
        // console.log(ele)
        isMoveMain = false;
        console.log(mappingValues)
        console.log(pages)
        setMappingValues();
        setPages()
      }
      
      return true;
    }
    if(value.id == "icon-delete-frame"){
      let datap = [];
      for( let i in pages[p - 1]){
          if(pages[p-1][i].id !== value.idFrame){
            datap.push(pages[p-1][i])
          }
      }
      pages[p - 1] = datap;
      datap = {}
      let c = 1;
      for( let i in mappingValues[p]){
          if(mappingValues[p][i].id !== value.idFrame){
            datap[p + "0000" + c] = mappingValues[p][i];
            c++;
          }
      }
      mappingValues[p] = datap;

      setPages(pages)
      setMappingValues(mappingValues)
      document.querySelectorAll(`[data-id="${value.idFrame}"]`)[0].remove();

      return true;
    }
    return false;
  }
  function handleEventDown(e) {
    if (!isTouch) {
      e.preventDefault();
    }
  
   
    timeOutMuti && clearTimeout(timeOutMuti);

    hideLabelFrame();

    // neu dang di chuyen 1 item, va dang di chuyen nen, thi ko duoc di chuyen nua
    if (isMove !== 0 && isMoveMain) {
      return;
    }


    let clientX;
    let clientY;
    if (isTouch) {
      clientX = e.touches[0].clientX;
      clientY = e.touches[0].clientY;
    } else {
      clientX = e.clientX;
      clientY = e.clientY;
    }

    const cbListenEvent = function () {

      if (isMove !== 0 && isMoveMain === false) {
        return;
      }

      window.removeEventListener('mousemove', handleEventMove, true);
      window.removeEventListener('touchmove', handleEventMove, true);
      if (isTouch) {
        window.addEventListener('touchmove', handleEventMove, true);
      } else {
        window.addEventListener('mousemove', handleEventMove, true);
      }
    }
    var hasClass = e.target.classList.contains('movable');
    if (!hasClass) {

      let isHandle = 2;


      if (isMove !== 0 || isMoveMain) {
        isHandle = 0;
      }

      if(isHandle !== 0 && e.target.id == "icon-edit-frame"){
        const dataP = e.target.parentElement.parentElement
        targetIconFrame = {id: e.target.id, idFrame: dataP.dataset.id, idDefault: dataP.id.replace("movable-", "") * 1};
      }
      if(isHandle !== 0 && e.target.id == "icon-pin-frame"){
        const dataP = e.target.parentElement.parentElement
        targetIconFrame = {id: e.target.id, idFrame: dataP.dataset.id, idDefault: dataP.id.replace("movable-", "") * 1};
      }
      if(isHandle !== 0 && e.target.id == "icon-delete-frame"){
        const dataP = e.target.parentElement.parentElement
        targetIconFrame = {id: e.target.id, idFrame: dataP.dataset.id, idDefault: dataP.id.replace("movable-", "") * 1};
      }
      if (isHandle !== 0 && e.target.id === 'pageNavigation') {

   
        hideVirtual(currentPage - 2, true, true);
        hideVirtual(currentPage - 1, true, true);
        hideVirtual(currentPage, true, true);
        hideVirtual(currentPage + 1, true, true);
        hideVirtual(currentPage + 2, true, true);


        isLockChangePage = true;

        isMovePageNavigation = true;

        pageInEventDown = currentPage;
        moveAddPosition = false;

        isHandle = true;
      }

      var hasClassAdd = isHandle !== 0 && e.target.classList.contains('add') ? true : false;
      if (hasClassAdd) {

        isMoveAdd = true;

        isHandle = true;


        // handle click to add

        const startRow = Math.floor((clientX - statusBarHeight) / (itemWidth + spaceWidth))
        const startCol = Math.floor((clientY) / (itemWidth + spaceWidth))

        moveAddPosition = {
          x: startRow,
          y: startCol,
          width: 1,
          height: 1,
        }

      }
      if (isHandle === true) {


        cbListenEvent();


        mainPos3 = clientX;
        mainPos4 = clientY;



        lastMainClientX = clientX;
        lastMainClientY = clientY;

        _target = e.target;

        return;
      }

    }
    if (!hasClass) {
      if (isMove != 0) {
        // neu cam ung, lay vi tri thu 2 
        if (isTouch && typeof e.touches[1] === 'object') {
          clientX = e.touches[1].clientX;
          clientY = e.touches[1].clientY;
        }
      }

      cbListenEvent();

      mainPos3 = clientX;
      mainPos4 = clientY;



      lastMainClientX = clientX;
      lastMainClientY = clientY;

      timeMoveX = performance.now();
      if (isPortrait) {
        lastScrollLeft = document.getElementById('main').scrollLeft;
      } else {
        lastScrollLeft = document.getElementById('main').scrollTop;
      }
      console.log('-------move main', lastScrollLeft);

      isMoveMain = true;


      // action touch
      if (!isEdit) {
        const hasClassNoMovebale = e.target.classList.contains('notMovable');
        if (hasClassNoMovebale) {
          timeOutMuti = setTimeout(() => {
            handleShowModalTouch(e.target)
            e.target.style.zIndex = 9999;
            handleShowTooltip(e.target)
            timeOutMuti = setTimeout(() => {
              toggleEdit()
              closeModal(e.target)
            }, 700)
          }, 700)
          return;
        }
      }

      return;
    }

    // ko cho di chuyen icon khi dang di chuyen icon khac
    if (isMove !== 0) {
      return;
    }
    cbListenEvent()

    console.log('==========isMove', isMove)

    isMove = 1;


    pos3 = clientX;
    pos4 = clientY;

    console.log('-----mouseDown--pos3: ', pos3);



    if (e.target.classList.contains('notInArea')) {
      mouseDownClientX = false;
    } else {
      mouseDownClientX = clientX;
      mouseDownClientY = clientY;
    }



    _target = e.target;

    zIndexNotInArea++;
    zIndex++;
    // // check class
    // if (e.target.classList.contains('notInArea')) {

    // } else {

    // }
    _target.style.zIndex = zIndexNotInArea;


    // get the mouse cursor position at startup:

  }

  // start: touch dapp
  const handleShowModalTouch = (ele) => {
    const node = document.createElement("DIV");
    node.id = "modal-touch"
    const handleStopPropagation = (e) => {
      e.stopPropagation();
    }
    node.addEventListener('touchstart', handleStopPropagation);
    node.addEventListener('mousedown', handleStopPropagation);
    node.addEventListener("click", () => {
      closeModal(ele)
    })
    document.getElementById('main').appendChild(node);

  }
  const handleShowTooltip = (ele) => {
    const { createPopper } = Popper;
    const node = document.createElement("DIV");
    node.id = "modal-touch_tooltip"
    const content = `<div class='modal-touch-item'><span>Add to Favorites</span>${addFavorite}</div><div 
    onclick='AddToGroup("${ele.dataset.id}")' class='modal-touch-item'><span>Add to Group </span> ${addToGroup}</div><div class='modal-touch-item' onclick='shareDapp("${ele.dataset.id}")'><span>Share</span> ${share}</div><div class='modal-touch-item delete'><span>Delete</span>${iconDelete}</div>`
    node.innerHTML = content;
    document.getElementById('main').appendChild(node);
    createPopper(ele, node, {
      placement: 'auto',

    });
  }
  const closeModal = (ele) => {
    document.getElementById('modal-touch')?.remove()
    document.getElementById('modal-touch_tooltip')?.remove()
    ele.style.zIndex = 1;
  }
  // end: touch dapp
  let isLockChangePage = false;

  var timeMoveX = 0;
  var pos1 = 0, _target, pos2 = 0, pos3 = 0, pos4 = 0, mainPos3 = 0, mainPos4 = 0, zIndex = 1, zIndexNotInArea = 1000000;

  var timeoutChangeSlide;

  var targetIconFrame = null;
  let lastLeft, lastTop;



  let lastClientX = -100, lastMainClientX = -100;
  let lastClientY = -100, lastMainClientY = -100;

  var targetShadow = null;
  var nodeTargetShadow = null;

  var timeOutShadow = null;
  function handleShowShadle (top, left, ele) {
    // console.log(ele)
    // let mappingDefaultValuesNew = {}
    // let mappingValuesNew = {}
    // if(ele.style.position === "fixed") {
    //   // mappingDefaultValuesNew = mappin
    // }else {
      
    // }
    console.log(mappingDefaultValues, mappingValues)
    if(targetShadow && nodeTargetShadow){
      nodeTargetShadow.remove()
    }
    const mappingNewValue = {};



    mappingNewValue.y = top;
    mappingNewValue.x = left;


    // const basicX = (currentPage - 1) * windowWidth;
    // const basicY = mappingNewValue.y;

    let basicX;
    let basicY;

    if (isPortrait) {
      basicX = mappingNewValue.x - (currentPage - 1) * window.innerWidth;
      basicY = mappingNewValue.y;
    } else {
      basicX = mappingNewValue.x;
      basicY = mappingNewValue.y - (currentPage - 1) * window.innerHeight;
    }
    let isExists = false;
    for (let n = 0; n < mappingDefaultValues.length; n++) {

      if (
        basicX < mappingDefaultValues[n].x - 20 ||
        basicX > mappingDefaultValues[n].x + 20
      ) {
        continue;
      }
      if (
        basicY < mappingDefaultValues[n].y - 20 ||
        basicY > mappingDefaultValues[n].y + 20
      ) {
        continue;
      }

      isExists = n;
      break;
    }
    if(!mappingDefaultValues[isExists]) return;
    if(isExists) {
      let matchPosition = false;
      const mappingTmp = {};
      if (isPortrait) {
        mappingTmp.x = mappingDefaultValues[isExists].x + (currentPage - 1) * window.innerWidth;
        mappingTmp.y = mappingDefaultValues[isExists].y
      } else {
        mappingTmp.x = mappingDefaultValues[isExists].x;
        mappingTmp.y = mappingDefaultValues[isExists].y + (currentPage - 1) * window.innerHeight;
      }
      mappingTmp.width = ele.offsetWidth
      mappingTmp.height = ele.offsetHeight
      const id = ele.id.replace("movable-", "") * 1;
      for (let n in mappingValues[currentPage]) {
        if (n == id) {
          continue;
        }

        if (typeof mappingValues[currentPage][n] !== 'object' || mappingValues[currentPage][n] === null || mappingValues[currentPage][n].notInSafe === true) {
          continue;
        }


        if (
          mappingTmp.x < mappingValues[currentPage][n].x &&
          mappingTmp.x + mappingTmp.width <= mappingValues[currentPage][n].x
        ) {
          continue;
        }
        if (
          mappingTmp.x >= mappingValues[currentPage][n].x + mappingValues[currentPage][n].width
        ) {
          continue;
        }


        if (
          mappingTmp.y < mappingValues[currentPage][n].y &&
          mappingTmp.y + mappingTmp.height <= mappingValues[currentPage][n].y
        ) {
          continue;
        }
        if (
          mappingTmp.y >= mappingValues[currentPage][n].y + mappingValues[currentPage][n].height
        ) {
          continue;
        }


        matchPosition = true;
        break;

      }
      
      if(matchPosition) return;
      const targetShadow2= document.querySelector(`#slide-${currentPage - 1} #grid-${mappingDefaultValues[isExists].row}-${mappingDefaultValues[isExists].col}`)
      if(targetShadow != targetShadow2){
          targetShadow = targetShadow2
          nodeTargetShadow = document.createElement("div");
          nodeTargetShadow.id = "shawdow-item"
          nodeTargetShadow.style.cssText  = `width: ${ele.offsetWidth}px; height: ${ele.offsetHeight}px`;
          targetShadow.appendChild(nodeTargetShadow)
      }else{
        targetShadow.appendChild(nodeTargetShadow)
      }
      
    
     
    }
  }
  function handleEventMove(e) {
    if (!isTouch) {
      e.preventDefault();
    }

    targetIconFrame = null;
    timeOutShadow && clearTimeout(timeOutShadow);
    timeOutMuti && clearTimeout(timeOutMuti);

    clearTimeout(timeoutChangeSlide);

    if (!isMoveMain && isMove == 1) {
      isMove = 2;
    }

    let clientX;
    let clientY;
    if (isTouch) {
      clientX = e.touches[0].clientX;
      clientY = e.touches[0].clientY;
    } else {
      clientX = e.clientX;
      clientY = e.clientY;
    }


    // move add
    if (isMoveAdd === true || isMovePageNavigation === true) {

      if (_target === null) {
        return;
      }

      let startX, endX;
      if (clientX > lastMainClientX) {
        startX = lastMainClientX;
        endX = clientX;
      } else {
        startX = clientX;
        endX = lastMainClientX;
      }

      let startY, endY;
      if (clientY > lastMainClientY) {
        startY = lastMainClientY;
        endY = clientY;
      } else {
        startY = clientY;
        endY = lastMainClientY;
      }

      if (isMovePageNavigation === true) {

        // lay tong so page
        let pageAllowTotal = pages.length;
        // let pageAllowTotal = Math.floor(pages.length / 2);

        if (pageAllowTotal > 20) {
          pageAllowTotal = 20;
        }

        let pageChange = pageInEventDown;

        if (isPortrait) {
          const widthPx = (window.innerWidth / 2) / pageAllowTotal;
          const compareSpaceX = endX - startX;
          const comparePageX = Math.floor(compareSpaceX / widthPx);

          if (clientX > lastMainClientX) {
            pageChange += comparePageX;
          } else {
            pageChange -= comparePageX;
          }
        } else {
          const heightPx = (window.innerHeight / 2) / pageAllowTotal;
          const compareSpaceY = endY - startY;
          const comparePageY = Math.floor(compareSpaceY / heightPx);

          if (clientY > lastMainClientY) {
            pageChange += comparePageY;
          } else {
            pageChange -= comparePageY;
          }
        }

        if (pageChange > pages.length) {
          pageChange = pages.length;
        } else if (pageChange < 1) {
          pageChange = 1;
        }

        if (pageChange === currentPage) {
          return;
        }


        // su dung bien moveAddPosition  bang true de biet co thay doi vi tri, con neu click thi show search
        moveAddPosition = true;

        currentPage = pageChange;

        setPageNumber();

        renderVirtual();

        scrollTo(((currentPage - 1) * windowWidth), function () {
          activePageInFrame(currentPage);
        }, 100);
        renderProssesBar()
        return;
      }

      // neu dang la dung, va vi tri keo o sap mep
      const eleParent = _target.parentNode.parentNode;
      const isMoveInFooter = eleParent.id === 'footer' ? true : false;
      // if (isMoveInFooter) {
      //   eleParent = _target.parentNode;
      // } else {
      //   eleParent = ;
      // }

      if (endX - startX < 30 && endY - startY < 30) {

        for (var i = 0; i < eleParent.childNodes.length; i++) {

          eleParent.childNodes[i].style.background = '';
        }

        moveAddPosition = {};
        return;
      }

      // quy doi ra so dong, cot theo chieu cao
      const startRow = Math.floor((startX - statusBarHeight) / (itemWidth + spaceWidth))
      const endRow = Math.ceil(endX / (itemWidth + spaceWidth));

      const startCol = Math.floor((startY) / (itemWidth + spaceWidth))
      const endCol = Math.ceil(endY / (itemWidth + spaceWidth));


      if (isMoveInFooter) {


        moveAddPosition = {
          x: startRow,
          y: 0,
          width: endRow - startRow,
          height: 1,
          type: 'footer',
        }

        for (var i = 0; i < numberColumn; i++) {

          const row = i;
          col = 0;

          const pos = row + col * numberColumn;
          const ele = eleParent.childNodes[pos];
          if (row < startRow || row >= endRow) {
            ele.style.background = '';
            continue;
          }

          ele.style.background = 'rgba(255, 255, 255, 0.32)';
        }
        return;
      }



      moveAddPosition = {
        x: startRow,
        y: startCol,
        width: endRow - startRow,
        height: endCol - startCol,
      }

      // doi ra vi tri o
      for (var i = 0; i < mappingDefaultValues.length; i++) {

        const mappingDefaultValue = mappingDefaultValues[i];

        const { col, row } = mappingDefaultValue;

        const pos = row + col * numberColumn;
        const ele = eleParent.childNodes[pos];
        if (row < startRow || row >= endRow) {
          ele.style.background = '';
          continue;
        }
        if (col < startCol || col >= endCol) {
          ele.style.background = '';
          continue;
        }

        ele.style.background = 'rgba(255, 255, 255, 0.32)';
      }
      return;
    }

    if (isMoveMain) {

      if (isMove != 0) {
        // neu cam ung, lay vi tri thu 2 
        if (isTouch && typeof e.touches[1] === 'object') {
          clientX = e.touches[1].clientX;
          clientY = e.touches[1].clientY;
        }
      }

      lastMainClientX = clientX;
      lastMainClientY = clientY;


      let spaceCompare = 0;
      let isMoveRight = false;

      if (isPortrait) {
        if (mainPos3 < clientX) {
          spaceCompare = clientX - mainPos3;
          isMoveRight = true;
        } else {
          spaceCompare = mainPos3 - clientX;
        }
      } else {
        if (mainPos4 < clientY) {
          spaceCompare = clientY - mainPos4;
          isMoveRight = true;
        } else {
          spaceCompare = mainPos4 - clientY;
        }
      }

      const top = lastTop;
      const left = lastLeft;

      if (spaceCompare < 10) {
        let isMoveDown = false;
  
        if ( isPortrait ) {
          if (mainPos4 < clientY) {
            spaceCompare = clientY - mainPos4;
            isMoveDown = true;
          } else {
            spaceCompare = mainPos4 - clientY;
          }
        } else {
          if (mainPos3 < clientX) {
            spaceCompare = clientX - mainPos3;
            isMoveDown = true;
          } else {
            spaceCompare = mainPos3 - clientX;
          }
        }
        console.log('move down', isMoveDown, spaceCompare);
  
        return;
  
      }
      if (isMoveRight) {
        let currentScroll;

        const compare = lastScrollLeft - spaceCompare;
        if (isPortrait) {
          currentScroll = document.getElementById('main').scrollLeft;
          document.getElementById('main').scrollLeft = compare;
          if (currentScroll === document.getElementById('main').scrollLeft) {
            currentScroll = false
          }

          if (currentScroll !== false) {
            moveFrame(spaceCompare, 0);
          }
        } else {
          currentScroll = document.getElementById('main').scrollTop;
          document.getElementById('main').scrollTop = compare;
          if (currentScroll === document.getElementById('main').scrollTop) {
            currentScroll = false
          }

          if (currentScroll !== false) {
            moveFrame(0, spaceCompare);
          }
        }

        // neu dang di chuyen item thi cong them vao
        if (currentScroll !== false && isMove !== 0) {
          if (isPortrait) {
            _target.style.left = (left - spaceCompare) + "px";
          } else {
            _target.style.top = (top - spaceCompare) + "px";
          }
        }

      } else {

        let currentScroll;

        const compare = lastScrollLeft + spaceCompare;

        if (isPortrait) {
          currentScroll = document.getElementById('main').scrollLeft;
          document.getElementById('main').scrollLeft = lastScrollLeft + spaceCompare;
          if (currentScroll === document.getElementById('main').scrollLeft) {
            currentScroll = false
          }

          if (currentScroll !== false) {
            moveFrame(spaceCompare * -1, 0);
          }
        } else {
          currentScroll = document.getElementById('main').scrollTop;
          document.getElementById('main').scrollTop = lastScrollLeft + spaceCompare;
          if (currentScroll === document.getElementById('main').scrollTop) {
            currentScroll = false
          }

          if (currentScroll !== false) {
            moveFrame(0, spaceCompare * -1);
          }
        }

        // neu dang di chuyen item thi cong them vao
        if (currentScroll !== false && isMove !== 0) {
          if (isPortrait) {
            _target.style.left = (left + spaceCompare) + "px";
          } else {
            _target.style.top = (top + spaceCompare) + "px";
          }
        }


      }
      return
    }

    if (_target === null) {
      return;
    }


    // calculate the new cursor position:
    pos1 = pos3 - clientX;
    pos2 = pos4 - clientY;
    pos3 = clientX;
    pos4 = clientY;

    // mainPos3 = clientX;
    // mainPos4 = clientY;


    const top = (_target.offsetTop - pos2);
    const left = (_target.offsetLeft - pos1);

    if(_target.dataset.pin != 1){
      timeOutShadow = setTimeout(() => {
        handleShowShadle(top, left, _target)
      },50)
    }

    lastTop = top;
    lastLeft = left;

    // neu diem di chuyen truoc va diem di chuyen sau nho hon 5px thi ko xu ly

    if (
      !isLockChangePage &&
      clientX + 5 > lastClientX && lastClientX < clientX + 5 &&
      clientY + 5 > lastClientY && lastClientY < clientY + 5
    ) {
      clearTimeout(timeoutChangeSlide);
      // 10 la mep border
      let isBack = false, isNext = false;

      let basicX;
      let basicY;

      if (isPortrait) {

        basicX = left - (currentPage - 1) * window.innerWidth;
        basicY = top;

        if (currentPage > 1 && left - window.innerWidth * (currentPage - 1) < 10) {
          isBack = true;
        } else if (currentPage < pages.length && left + _target.clientWidth + 10 >= window.innerWidth * currentPage) {
          isNext = true;
        }

      } else {

        basicX = left;
        basicY = top - (currentPage - 1) * window.innerHeight;

        if (currentPage > 1 && top - window.innerHeight * (currentPage - 1) < 10) {
          isBack = true;
        } else if (currentPage < pages.length && top + _target.clientHeight + 10 >= window.innerHeight * currentPage) {
          isNext = true;
        }

      }

      if (isBack || isNext) {

        // console.log('----------move', isPortrait, currentPage, pages.length, top, _target.clientHeight, 'windowHeight', windowHeight, window.innerHeight, isBack, isNext )


        repeatSwipe(isBack, left, top);
        // }
      } else {
      }

    }

    lastClientX = clientX;
    lastClientY = clientY;
    // if (top < 0 || top > windowHeight) {
    //   return;
    // }
    // set the element's new position:
    _target.style.top = top + "px";
    _target.style.left = left + "px";


  }

  function repeatSwipe(isBack, left, top) {

    clearTimeout(timeoutChangeSlide);

    timeoutChangeSlide = setTimeout(() => {

      console.log('next-slide', currentPage, isLockChangePage);
      if (_target === null || typeof _target !== 'object') {
        console.log("e.target.scrollLeft", "null");
        return;
      }

      if (isLockChangePage) {
        return;
      }

      isLockChangePage = true;

      const lastPage = currentPage;

      let newLeft, newTop;
      // document.getElementById('main').style.overflowX = 'scoll'
      if (isBack) {
        if (isPortrait) {
          newLeft = (left - window.innerWidth);
          _target.style.left = newLeft + "px";
        } else {
          newTop = (top - window.innerHeight);
          _target.style.top = newTop + "px";
        }

        currentPage--;

        // Swipes to the next slide

      } else {
        if (isPortrait) {
          newLeft = (left + window.innerWidth);
          _target.style.left = newLeft + "px";
        } else {
          newTop = (top + window.innerHeight);
          _target.style.top = newTop + "px";
        }

        currentPage++;
      }

      if (currentPage > pages.length) {
        currentPage = pages.length;
      } else if (currentPage < 1) {
        currentPage = 1;
      }

      if (lastPage === currentPage) {
        isLockChangePage = false
        return;
      }

      setPageNumber();
      // doi  vi tri page cho doi tuong
      const id = _target.id.replace("movable-", "") * 1;
      mappingValues[currentPage][id] = { ...mappingValues[lastPage][id] }
      mappingValues[lastPage][id] = null;
      delete mappingValues[lastPage][id];

      setMappingValues();

      changeClassName(_target, id);

      renderVirtual();

      scrollTo(((currentPage - 1) * windowWidth), function () {
        isLockChangePage = false
        activePageInFrame(currentPage);

        if (currentPage >= pages.length) {
          return;
        } else if (currentPage <= 1) {
          return;
        }

        timeoutChangeSlide = setTimeout(function () {
          repeatSwipe(isBack, newLeft, newTop)
        }, 1000)
      }, 300);
      // scrollTo(((currentPage - 1) * windowWidth), function() {
      //   isLockChangePage = false
      //   activePageInFrame(currentPage);
      // }, 300);

    }, 900)
  }

  let timeout_wheel = 0;;
  const wheelSlide = function (e) {

    e.preventDefault();

    if (isLockChangePage) {
      return;
    }
    isLockChangePage = true;
    let isBack = false;
    if (isPortrait) {

      if (e.deltaX < 0) {
        isBack = true;
      }
    } else {

      if (e.deltaY < 0) {
        isBack = true;
      }
    }

    const lastPage = currentPage;
    if (isBack) {
      currentPage--;
    } else {
      currentPage++;
    }
    if (currentPage > pages.length) {
      currentPage = pages.length;
    } else if (currentPage < 1) {
      currentPage = 1;
    }

    if (lastPage === currentPage) {
      isLockChangePage = false
      return;
    }
    if (_target !== null && typeof _target === 'object') {
      changeClassName(_target, _target.id);
    }

    setPageNumber();
    clearTimeout(timeout_wheel);
    renderVirtual()

    scrollTo(((currentPage - 1) * windowWidth), () => {
      clearTimeout(timeout_wheel);
      timeout_wheel = setTimeout(() => {
        isLockChangePage = false
        activePageInFrame(currentPage);
      }, 800)

    }, 300);
  }
  // document.body.addEventListener("wheel", wheelSlide);
  // document.body.addEventListener("mousewheel", wheelSlide);

  document.getElementById('content').addEventListener("wheel", wheelSlide);
  document.getElementById('content').addEventListener("mousewheel", wheelSlide);

  if (isTouch) {
    document.getElementById('main').addEventListener('touchstart', handleEventDown, false);
    window.addEventListener('touchend', handleEventUp, false);
  } else {
    document.getElementById('main').addEventListener('mousedown', handleEventDown, false);
    window.addEventListener('mouseup', handleEventUp, false);
  }


  function setBackground(obj) {
    /*
  
    window.postMessage({
      action: 'setBackground',
      url: 'https://trygalaxy.com/_next/static/media/try-galaxy-background.9da3bc9f.jpg',
      repeat: 'no-repeat',
      position: 'right 0',
      size: 'auto 160%'
    }, '*')
  
    */
    const { url, repeat, position, size, color } = obj;


    if (url === "") {
      document.getElementById('main').style.backgroundImage = "";
    } else {
      document.getElementById('main').style.backgroundImage = `url("${url}")`
    }
    document.getElementById('main').style.backgroundColor = color
    document.getElementById('main').style.backgroundRepeat = repeat
    document.getElementById('main').style.backgroundPosition = position
    document.getElementById('main').style.backgroundSize = size

    console.log('======', document.getElementById('main').style.backgroundImage)
  }


  function addIten(obj) {
    /*
  
    window.postMessage({
      action: 'addIten',
      name: 'Clumsy Vird',
      icon: 'https://images.emulatorgames.net/super-nintendo/sd-gundam-g-next-rom-pack-collection.jpg',
      // backgroundColor: 'red',addIten
      position: {
        x: 0,
        y: 5,
        width: 1,
        height: 1
      },
      page: 1
    }, '*')
  
    */
    const { name, icon, backgroundColor, position, page } = obj;

    // const { x, y, width, height } = position;


    pages[page - 1].push({
      name, icon,
      position: { ...position }
    })
    setPages();


    isForceRenderVirtual = true;
    // goi ham render
    renderVirtual();

    // xet _target va xu ly 
    const i = pages[page - 1].length;
    const totalItem = page * 100000 + i;

    isMove = 2;
    _target = document.getElementById(`movable-${totalItem}`)

    zIndex++;
    processMouseUp(_target, 1);

    isForceRenderVirtual = false;
  }


  function delIten(obj) {
    /*
  
    window.postMessage({
      action: 'delIten',
      index: 1,
      page: 1
    }, '*')
  
    */
    const { index, page } = obj;

    // const { x, y, width, height } = position;

    // chuyen tu vi tri qua index
    isForceRenderVirtual = true;
    pages[page - 1][index] = null;
    setPages();


    // goi ham render
    renderVirtual();
    isForceRenderVirtual = false;
  }
  function delItenFromXY(obj) {
    /*
  
    window.postMessage({
      action: 'delItenFromXY',
      position: {
        x: 0,
        y: 5,
      },
      page: 1
    }, '*')
  
    */
    const { position, page } = obj;

    const { x, y } = position;

    const index = 0;

    // chuyen tu vi tri qua index
    return delItem(index)
  }


  function cancelEdit() {
    localStorage.removeItem('pages');
    localStorage.removeItem('itemInFooters');


    localStorage.removeItem('customItemInFooters');
    localStorage.removeItem('mappingFooterValues');
    localStorage.removeItem('currentPage');
    localStorage.removeItem('mappingValues');

    localStorage.setItem('isEdit', 0);

    setPageNumber();
  }

  function saveEdit() {

    const eles = document.getElementsByClassName("notInArea");
    if (eles.length > 0) {
      const ele = eles[0];

      // lay page 
      let lastPage = -1;
      ele.classList.forEach(function (className) {
        if (className.indexOf("movable-") === -1) {
          return;
        }
        lastPage = className.replace("movable-", '') * 1 + 1;
      })
      if (alert(`has not safe in page ${lastPage} ?`)) {
        return;
      }
    }

    // show data

    console.log('--------data', [...pages]);
    console.log('--------data', [...itemInFooters]);

    // postmessage

    window.postMessage({
      pages: [...pages],
      itemInFooters: [...itemInFooters],
    })

    cancelEdit();
  }

  window.onerror = function (event, source, lineno, colno, error) {

    console.log('====error======', event, source, lineno, colno, error)

    if (isDebug) {
      alert(`error: line: ${lineno}, col: ${colno}, msg: ${error}`);
      return;
    }

    cancelEdit();

    window.location.reload();
  }

  // 
  const RenderAddFrame = (data) => {
    const  { moveAddPosition, name, url,id } = data;

    let matchPosition = false;

    let xNative = moveAddPosition.x * itemWidth + spaceWidth * (moveAddPosition.x + 1);
    const x = (currentPage - 1) * windowWidth + xNative;

    const y = statusBarHeight + moveAddPosition.y * itemWidth + spaceWidth * (moveAddPosition.y + 1);

    const width = moveAddPosition.width * (itemWidth) + (moveAddPosition.width - 1) * spaceWidth;
    const height = moveAddPosition.height * itemWidth + (moveAddPosition.height - 1) * spaceWidth;
    for (var n in mappingValues[currentPage]) {
      // if (n == id) {
      //   continue;
      // }

      if (typeof mappingValues[currentPage][n] !== 'object' || mappingValues[currentPage][n] === null || mappingValues[currentPage][n].notInSafe === true) {
        continue;
      }


      if (
        x < mappingValues[currentPage][n].x &&
        x + width <= mappingValues[currentPage][n].x
      ) {
        continue;
      }
      if (
        x >= mappingValues[currentPage][n].x + mappingValues[currentPage][n].width
      ) {
        continue;
      }


      if (
        y < mappingValues[currentPage][n].y &&
        y + height <= mappingValues[currentPage][n].y
      ) {
        continue;
      }
      if (
        y >= mappingValues[currentPage][n].y + mappingValues[currentPage][n].height
      ) {
        continue;
      }
      matchPosition = true;
      break;

    }
    
    if(matchPosition){
      alert("trung")
      const dataItem = {
        id: id,
        name: name,
        content: "",
        type: 3,
        options: {
          url: url,
        },
        position: {
          x: moveAddPosition.x,
          y: moveAddPosition.y,
          width: moveAddPosition.width,
          height: moveAddPosition.height,
        },
      }
      

      
      let dataCurrentPage = pages[currentPage - 1];
      dataCurrentPage.push(dataItem)
      pages[currentPage - 1] = dataCurrentPage;
      setPages()
      // const xNative = dataItem.position.x * itemWidth + spaceWidth * (dataItem.position.x + 1);
      // const x = (currentPage - 1) * windowWidth + xNative;

      // const y = statusBarHeight + dataItem.position.y * itemWidth + spaceWidth * (dataItem.position.y + 1);
 
      // const width = dataItem.position.width * (itemWidth) + (dataItem.position.width - 1) * spaceWidth;
      // const height = dataItem.position.height * itemWidth + (dataItem.position.height - 1) * spaceWidth;
    
    mappingValues[currentPage][currentPage + '0000' + (dataCurrentPage.length)] = {
      x, y, width,height, 
      options: dataItem.options,
      type: dataItem.type,
      notInSafe: true,
      id: id
    }
    setMappingValues()
    renderVirtualPrev();
    renderGridMain();
    renderGridFooter();
    removeAllFrame()
    }else {
      const dataItem = {
        id: id,
        name: name,
        content: "",
        type: 3,
        options: {
          url: url,
        },
        position: {
          x: moveAddPosition.x,
          y: moveAddPosition.y,
          width: moveAddPosition.width,
          height: moveAddPosition.height,
        },
      }

      let dataCurrentPage = pages[currentPage - 1];
      dataCurrentPage.push(dataItem)
      pages[currentPage - 1] = dataCurrentPage;
      setPages()
        // xNative = dataItem.position.x * itemWidth + spaceWidth * (dataItem.position.x + 1);
        // const x = (currentPage - 1) * windowWidth + xNative;

        // const y = statusBarHeight + dataItem.position.y * itemWidth + spaceWidth * (dataItem.position.y + 1);
   
        // const width = dataItem.position.width * (itemWidth) + (dataItem.position.width - 1) * spaceWidth;
        // const height = dataItem.position.height * itemWidth + (dataItem.position.height - 1) * spaceWidth;
      
      mappingValues[currentPage][currentPage + '0000' + (dataCurrentPage.length)] = {
        x, y, width,height, 
        options: dataItem.options,
        type: dataItem.type,
        id: id
      }
      setMappingValues()
      renderVirtualPrev();
      renderGridMain();
      renderGridFooter();
      removeAllFrame()
    }
  }
  const RenderAddDapp = (data) => {
    const  { moveAddPosition, name,id, logo } = data;
    let xNative = moveAddPosition.x * itemWidth + spaceWidth * (moveAddPosition.x + 1);
    const x = (currentPage - 1) * windowWidth + xNative;
    const y = (moveAddPosition.y * itemWidth +  (moveAddPosition.y + 1) * spaceWidth) + statusBarHeight
    const width = moveAddPosition.width * (itemWidth) + (moveAddPosition.width - 1) * spaceWidth;
    const height = moveAddPosition.height * itemWidth + (moveAddPosition.height - 1) * spaceWidth;
    const dataItem  = {
      "name": name,
      "logo": logo,
      "id": id,
      "type": 1,
      position: {
        x: moveAddPosition.x,
        y: moveAddPosition.y,
        width: moveAddPosition.width,
        height: moveAddPosition.height,
      },
    }
   
    let dataCurrentPage = pages[currentPage - 1];
    dataCurrentPage.push(dataItem)
    pages[currentPage - 1] = dataCurrentPage;
    setPages()
    mappingValues[currentPage][currentPage + '0000' + (dataCurrentPage.length)] = {
      x, y, width,height, 
      options: dataItem.options,
      type: dataItem.type,
      id: id
    }
    setMappingValues();
    renderVirtualPrev()
    renderGridMain();
    renderGridFooter();
    removeAllFrame()
    // let html = ''

    // let widthItem = Math.floor(width * 0.7) - 2;
    // const spaceItem = Math.floor((width - widthItem) / 4); 
    // widthItem = width - spaceItem * 4;
    // const content = renderHtmlDapp(dataItem, spaceItem, widthItem, width, fontSize);
    // html += `<div class="warning"></div>
    // <div class="content-item" style='background: ${dataItem.background}; width: ${width}px; height:${height}px;pointer-events: none;text-align: center;position: relative;overflow: hidden; border-radius: 18px'>
    //   ${content}
    // </div>
    // `
    // // ${dataItem.type == 3 ? handleShowLabel(dataItem.name, width) + dataItem.content: "" }
    // let className = dataItem.type == 3?"item-frame": ''
    // var node = document.createElement("DIV");
    // if (isEdit) {
    //   className += ` movable movable-${3123123}`
    // } else {
    //   className += ` notMovable movable-${13123123}`
    // }
    // node.className = className;
    // node.id = `movable-${3213213}`
    // node.dataset.id = dataItem.id
    // node.dataset.type = dataItem.type
    // node.style = `z-index: ${zIndex};position:absolute; left:${x}px; top:${y}px; width:${width}px; height:${height}px;`
    // node.innerHTML = html
    // document.getElementById('content').appendChild(node);
    // console.log(pages[currentPage - 1])
    // console.log(mappingValues[currentPage])
  }
  const SetFrameData = (data) => {
    console.log(data);
  
    const elementFrame = document
       .querySelector(`[data-id="${data.id}"]`)
       .getAttribute("id")
       .split("-")[1];
    const index = pages[currentPage - 1].findIndex((obj) => {
       return obj.id === `${data.id}`;
    });
    const dataItem = {
      id: "23123",
      name: "hello keytiii",
      content: "",
      type: 3, // web | dapp
      options: {
         url: data.url,
      },
      position: {...data.position, width: data.style.width, height: data.style.height},
      style: {
         area: {
            height: 100,
            width: 100,
         },
         bg: "#000",
         bg2: "#000",
         bgStyle: {
            left: 0,
            rotate: 0,
            scale: 0,
            top: 0,
         },
         isFront: true,
         mixBlend: "soft-light",
         mixBlend2: "unset",
         name: {
            bg: "#000",
            color: "#fff",
            opacityBg: 100,
            opacityText: 100,
            text: "hello keytiii",
         },
         opacity: 100,
         opacity2: 100,
         round: 100,
         size: {
            height: 2,
            width: 2,
         },
      },
   };
    pages[currentPage - 1][index] = data;
    // document.body.innerHTML = JSON.stringify(pages[currentPage - 1][index])

    let xNative = data.position.x * itemWidth + spaceWidth * ( data.position.x + 1);
    const x = (currentPage - 1) * windowWidth + xNative;

    const y = statusBarHeight + data.position.y * itemWidth + spaceWidth * (data.position.y + 1);
    mappingValues[currentPage][elementFrame] = {
      ... mappingValues[currentPage][elementFrame],
      x: x, y: y, style: data.style

    };
    
    // // window.localStorage.setItem("initial-pages", JSON.stringify(pages));
    // mappingValues[currentPage][elementFrame] = {
    //    height: 180,
    //    width: 180,
    //    type: 3,
    //    x: 102,
    //    y: 36,
    //    options: {
    //       url: "https://www.youtube.com/embed/56xBfYg5neE",
    //    },
    //    style: {
    //       area: {
    //          height: 100,
    //          width: 100,
    //       },
    //       bg: "#fff",
    //       bg2: "#000",
    //       bgStyle: {
    //          left: 0,
    //          rotate: 0,
    //          scale: 0,
    //          top: 0,
    //       },
    //       isFront: true,
    //       mixBlend: "soft-light",
    //       mixBlend2: "unset",
    //       name: {
    //          bg: "#000",
    //          color: "#fff",
    //          opacityBg: 100,
    //          opacityText: 100,
    //          text: "hello keytiii",
    //       },
    //       opacity: 100,
    //       opacity2: 100,
    //       round: 100,
    //       size: {
    //          height: 2,
    //          width: 2,
    //       },
    //    },
    // };
    setPages()
    setMappingValues();
    renderVirtualPrev();
    renderGridMain();
    renderGridFooter();
    removeAllFrame();
  };
  var n = (e) => {
    console.log('---hello---start-receive--url', typeof e.data, e.data);

    if (typeof e.data !== 'object' || e.data === null) {
      return;
    }

    const action = e.data.command;
    const data = e.data.data; //success, data
    switch(action){
      case 'on-end-add-frame':{
        if(data.success){
          RenderAddFrame(data.data);
        }
        return;
      }
      case 'on-end-add-d-app':{
        if(data.success){
          RenderAddDapp(data.data);
        }
        return;
      }
      case 'on-end-edit-frame':{
        if(data.success){
          SetFrameData(data.data);
        }
        return;
      }
      default: 
      return;

    }
    // if (action === 'setBackground') {
    //   return setBackground(e.data);
    // }

    // if (action === 'addIten') {
    //   return addIten(e.data);
    // }

    // if (action === 'delIten') {
    //   return delIten(e.data);
    // }


    // if (action === 'cancelEdit') {
    //   return cancelEdit();
    // }
    // if (action === 'saveEdit') {
    //   return saveEdit();
    // }
  };

  window.addEventListener("message", n, !1);

  
  // send to native
  var Send = (payload) => {
    window.webkit?.messageHandlers.callbackHandler.postMessage(
      JSON.stringify(payload)
    );

  }
  var eleMenu = document.getElementById('menuframe_list');
  function handleEventDownMenu (e){
    if (isTouch) {
      pos3 = e.touches[0].clientX;
      pos4 = e.touches[0].clientY;
    } else {
      pos3 = e.clientX;
      pos4 = e.clientY;
    }
  }
  function handleEventUpMenu (e){
    console.log(e)
  }
  function handleEventMoveMenu(e){
    let clientX;
    let clientY;
    if (isTouch) {
      clientX = e.touches[0].clientX;
      clientY = e.touches[0].clientY;
    } else {
      clientX = e.clientX;
      clientY = e.clientY;
    }
    pos1 = pos3 - clientX;
    pos2 = pos4 - clientY;
    pos3 = clientX;
    pos4 = clientY;

    // mainPos3 = clientX;
    // mainPos4 = clientY;

    const top = (eleMenu.offsetTop - pos2);
    const left = (eleMenu.offsetLeft - pos1);
    eleMenu.style.top = top + "px";
    eleMenu.style.left = left + "px";
  }
  // move menu 
  
  if (isTouch) {
    eleMenu.addEventListener('touchstart', handleEventDownMenu);
    eleMenu.addEventListener('touchend', handleEventUpMenu);
    window.addEventListener('touchmove', handleEventMoveMenu);
  } else {
    eleMenu.addEventListener('mousedown', handleEventDownMenu);
    eleMenu.addEventListener('mouseup', handleEventUpMenu);
    eleMenu.addEventListener('mousemove', handleEventMoveMenu);
  }
})()



