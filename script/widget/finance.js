const urlParams = new URLSearchParams(window.location.search);
const w = urlParams.get("w");
const h = urlParams.get("h");
const isSmall = Number(w) === 2 && Number(h) === 2;
const isMedium = Number(w) === 4 && Number(h) === 2;

console.log("window.appId -->", window.appId);

window.webkit.messageHandlers.callbackHandler.postMessage(
  JSON.stringify({
    command: "check-is-online",
    appId: window.appId,
  })
);

const createLoadingScreen = () => {
  const style = document.createElement("style");
  document.head.appendChild(style);

  // Define keyframes dynamically
  const keyframes = `
                        @keyframes spin {
                            0% { transform: rotate(0deg); }
                            100% { transform: rotate(360deg); }
                        }`;

  const rule1 = `#loadingContainer {
                            display: flex;
                            position: fixed;
                            top: 0;
                            left: 0;
                            width: 100vw;
                            height: 100vh;
                            background-color: rgba(255, 255, 255, 1);
                            justify-content: center;
                            align-items: center;
                            z-index: 1000;
                            border-radius: 20px
                        }`;

  const rule2 = `#loadingSpinner {
                            border: 4px solid #f3f3f3;
                            border-top: 4px solid rgba(14, 14, 14, 1);
                            border-radius: 50%;
                            width: 40px;
                            height: 40px;
                            animation: spin 1s linear infinite;
                        }`;

  // Add keyframes to the style tag
  style.sheet.insertRule(keyframes, 0);
  style.sheet.insertRule(rule1, 0);
  style.sheet.insertRule(rule2, 0);

  const container_div = document.createElement("div");
  container_div.id = "loadingContainer";
  const spinner_div = document.createElement("div");
  spinner_div.id = "loadingSpinner";

  document.body.appendChild(container_div);
  container_div.appendChild(spinner_div);
};

const getData = () => {
  createLoadingScreen();

  setTimeout(() => {
    let arrObjStock = [];

    let symbolList = document.querySelectorAll('td[aria-label="Symbol"]');
    let chartList = document.querySelectorAll('td[aria-label="Day Chart"]');
    let nameList = document.querySelectorAll('td[aria-label="Name"]');
    let lastPriceList = document.querySelectorAll(
      'td[aria-label="Last Price"]'
    );
    console.log(lastPriceList);
    let changeList = document.querySelectorAll('td[aria-label="Change"]');

    for (let i = 0; i < symbolList.length; i++) {
      arrObjStock.push({
        symbol: symbolList[i].querySelector("a").innerText,
        chartImg: chartList[i]
          .querySelector("a")
          .querySelector("canvas")
          .toDataURL("image/png"),
        nameStock: nameList[i].innerText,
        lastPrice: lastPriceList[i].querySelector("fin-streamer").innerText,
        change: changeList[i]
          .querySelector("fin-streamer")
          .querySelector("span").innerText,
      });
    }

    window.webkit.messageHandlers.callbackHandler.postMessage(
      JSON.stringify({
        command: "write-to-local-storage",
        appId: window.appId,
        value: {
          key: "stock",
          data: arrObjStock,
        },
      })
    );

    // window.location.href = `https://img.m.pro/widgets/other/#/stock-neutral?w=${w}&h=${h}`;
    exitWebview();
  }, 2000);
};

const exitWebview = () => {
  window.webkit.messageHandlers.callbackHandler.postMessage(
    JSON.stringify({
      command: "exit-and-reload-data",
      appId: window.appId,
    })
  );
};

window.addEventListener("message", (e) => {
  console.log("message from native", e);
  switch (e.data.command) {
    case "check-is-online": {
      if (e.data.data.data.isOnline) {
        if (
          window.location.href.includes(
            "https://finance.yahoo.com/trending-tickers/"
          )
        ) {
          getData();
        } else {
          const a = document.querySelector("#loadingContainer");
          if (a) a.zIndex = "-1";
        }
      } else {
        window.location.href = `https://img.m.pro/widgets/other/#/news?w=${w}&h=${h}&old=1`;
      }
    }
    default:
      break;
  }
});
