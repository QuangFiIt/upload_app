const urlParams = new URLSearchParams(window.location.search);
const w = urlParams.get("w");
const h = urlParams.get("h");
console.log(`run script news with = ${w} and height = ${h}`);

window.webkit.messageHandlers.callbackHandler.postMessage(
  JSON.stringify({
    command: "check-is-online",
    appId: window.appId,
  })
);

console.log("uuuuuuuuuuuuuuuuuu");

const readDataLocal = () => {
  window.webkit.messageHandlers.callbackHandler.postMessage(
    JSON.stringify({
      command: "read-from-local-storage-content-news",
      appId: window.appId,
      value: {
        key: "content-news",
        id: "",
      },
    })
  );
};

const writeDataLocal = (contentNews) => {
  console.log("content", contentNews);
  window.webkit.messageHandlers.callbackHandler.postMessage(
    JSON.stringify({
      command: "write-to-local-storage",
      appId: window.appId,
      value: {
        key: "content-news",
        data: contentNews,
        // data: ["Hoang ga"],
      },
    })
  );
};

function resizeImageBase64(file, maxWidth, maxHeight) {
  return new Promise((resolve, reject) => {
    var img = new Image();
    img.onload = function () {
      var canvas = document.createElement("canvas");
      var width = img.width;
      var height = img.height;

      // Tính toán tỉ lệ giảm kích thước
      if (width > height) {
        if (width > maxWidth) {
          height *= maxWidth / width;
          width = maxWidth;
        }
      } else {
        if (height > maxHeight) {
          width *= maxHeight / height;
          height = maxHeight;
        }
      }

      // Resize ảnh bằng canvas
      canvas.width = width;
      canvas.height = height;
      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0, width, height);

      // Chuyển canvas thành Base64
      var resizedBase64 = canvas.toDataURL(file.type || "image/png");
      resolve(resizedBase64);
    };
    img.onerror = reject;
    img.src = URL.createObjectURL(file);
  });
}

const imageToBase64 = (url, callback) => {
  console.log("url", url);
  fetch(url)
    .then((response) => response.blob())
    .then((blob) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob);
      reader.onloadend = () => {
        const base64String = reader.result;
        callback(base64String);
      };
    })
    .catch((err) => {
      console.log(err);
    });
};

const createLoadingScreen = () => {
  const style = document.createElement("style");
  document.head.appendChild(style);

  // Define keyframes dynamically
  const keyframes = `
                        @keyframes spin {
                            0% { transform: rotate(0deg); }
                            100% { transform: rotate(360deg); }
                        }`;

  const rule1 = `#loadingContainer {
                            display: flex;
                            position: fixed;
                            top: 0;
                            left: 0;
                            width: 100vw;
                            height: 100vh;
                            background-color: rgba(255, 255, 255, 1);
                            justify-content: center;
                            align-items: center;
                            z-index: 1000;
                            border-radius: 20px
                        }`;

  const rule2 = `#loadingSpinner {
                            border: 4px solid #f3f3f3;
                            border-top: 4px solid rgba(14, 14, 14, 1);
                            border-radius: 50%;
                            width: 40px;
                            height: 40px;
                            animation: spin 1s linear infinite;
                        }`;

  // Add keyframes to the style tag
  style.sheet.insertRule(keyframes, 0);
  style.sheet.insertRule(rule1, 0);
  style.sheet.insertRule(rule2, 0);

  const container_div = document.createElement("div");
  container_div.id = "loadingContainer";
  const spinner_div = document.createElement("div");
  spinner_div.id = "loadingSpinner";

  document.body.appendChild(container_div);
  container_div.appendChild(spinner_div);
};

const getData = () => {
  // createLoadingScreen();
  console.log("start getdata");
  let i = 1;
  const id2 = setInterval(() => {
    if (i > 10) {
      clearInterval(id2);
    }
    window.scrollTo(0, 500 * i);
    console.log("i", i);
    i++;
  }, 500);

  const content_news = [];

  setTimeout(() => {
    let arrPromise = [];

    const article_list = document.querySelectorAll("article");
    console.log("article_list", article_list);

    for (let j = 0; j < article_list.length; j++) {
      let src, srcset, title, urll, icon, brand;
      if (
        article_list[j].querySelector("figure") &&
        article_list[j].querySelector("figure").querySelector("img") &&
        article_list[j].querySelector("figure").querySelector("img").src
      ) {
        src = article_list[j].querySelector("figure").querySelector("img").src;
        srcset =
          "https://news.google.com" +
          article_list[j]
            .querySelector("figure")
            .querySelector("img")
            .srcset.split(",")[1]
            .trim()
            .split(" ")[0];
      }

      if (
        article_list[j].querySelector("h4") &&
        article_list[j].querySelector("h4").querySelector("a") &&
        article_list[j].querySelector("h4").querySelector("a").innerText &&
        article_list[j].querySelector("h4").querySelector("a").href
      ) {
        title = article_list[j]
          .querySelector("h4")
          .querySelector("a").innerText;
        urll = article_list[j].querySelector("h4").querySelector("a").href;
      }

      if (
        article_list[j].querySelector("div") &&
        article_list[j].querySelector("div").querySelector("img") &&
        article_list[j].querySelector("div").querySelector("img").src
      ) {
        icon = article_list[j].querySelector("div").querySelector("img").src;
      }

      if (
        article_list[j].querySelector("div") &&
        article_list[j].querySelector("div").querySelector("div") &&
        article_list[j]
          .querySelector("div")
          .querySelector("div")
          .querySelector("a") &&
        article_list[j]
          .querySelector("div")
          .querySelector("div")
          .querySelector("a").innerText
      ) {
        brand = article_list[j]
          .querySelector("div")
          .querySelector("div")
          .querySelector("a").innerText;
      }

      if ((srcset, title, urll, icon, brand)) {
        arrPromise.push(
          Promise.all([
            new Promise((res) => {
              imageToBase64(srcset, (base64String) => {
                res(base64String);
              });
            }),
            // new Promise((res) => {
            //   imageToBase64(icon, (base64String) => {
            //     res(base64String);
            //   });
            // }),
          ]).then(([src]) => {
            content_news.push({
              src,
              title,
              url: urll,
              icon,
              brand,
            });
          })
        );
      }
    }

    Promise.race(
      [
        Promise.all(arrPromise)
          .then(() => {
            console.log("writeDataLocal", content_news);
            writeDataLocal(content_news);
            console.log("end get data");
          })
          .catch((err) => {
            console.log(err);
          }),
      ],
      new Promise((resolve, reject) => {
        setTimeout(() => {
          reject(new Error("Promise timed out"));
        }, 10000);
      })
    );
  }, 6000);
};

const exitWebview = () => {
  console.log("window.appId", window.appId);
  window.webkit.messageHandlers.callbackHandler.postMessage(
    JSON.stringify({
      command: "exit-and-reload-data",
      appId: window.appId,
    })
  );
};

window.addEventListener("message", (e) => {
  console.log(e.data.command, e);
  switch (e.data.command) {
    case "check-is-online": {
      console.log("go in case check-is-online", e.data.data.data.isOnline);
      if (e.data.data.data.isOnline) {
        if (window.location.href.includes("https://news.google.com/")) {
          getData();
        } else {
          const a = document.querySelector("#loadingContainer");
          if (a) a.zIndex = "-1";
        }
      } else {
        window.location.href = `https://img.m.pro/widgets/other/#/news?w=${w}&h=${h}&old=1`;
      }
      break;
    }
    case "write-to-local-storage": {
      if (e.data.data.success) {
        readDataLocal();
      } else {
        console.log("Write data to local fail");
      }
      break;
    }
    case "read-from-local-storage-content-news": {
      if (e.data.data.data.length > 0) {
        // window.location.href = `https://img.m.pro/widgets/other/#/news?w=${w}&h=${h}&old=0`;
        exitWebview();
      } else {
        getData();
      }
      break;
    }
    default:
      break;
  }
});

// url https://news.google.com/api/attachments/CC8iL0NnNHhVazlxU0U5b1ExQTFTMmN6VFJEQUF4aXRCU2dLTWdtQllaYnpxQ2pnN1FF=-w700-h350-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iK0NnNUdWMVJMZWxGeVYxZHJZVTlQVFJEREF4aW9CU2dLTWdhVlFJVHJ6UUk=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iK0NnNW9RWHB4TUROeldYUjZWMm80VFJERUF4aW1CU2dLTWdhQnNZYlR6QU0=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iK0NnNVdVakEwTFc1S2VrUmxZMVJqVFJDUUF4allCQ2dLTWdZWm81Q3JQUVk=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iJ0NnNHpWemxwV2tkRE9VMW5hbk4yVFJERUF4aW1CU2dLTWdNcEFndw=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iK0NnNW5ZVUpWYkZsWVJUVmxlVEJJVFJEZ0F4aUFCU2dLTWdZQmdZZ3BTUVE=-w700-h350-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iL0NnNXdhRUpTY2xwNldFVXROVFZvVFJDTkFSajZBU2dLTWdrSllvVEZKbWc5U1FJ=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iL0NnNDNaVTFqYXpKeFRtNXBaMFV4VFJDZkF4ampCU2dLTWdrZEU0Z3FtdXFTTkFF=-w700-h350-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iL0NnNTRZelZQWVRsMlVVRnVRbFJDVFJDZkF4ampCU2dLTWdtTk1aU21HcW8xQ3dJ=-w700-h350-p-df-rw
// VM461:41 url https://news.google.comhttps://img.youtube.com/vi/2pMc8NOd2Xw/0.jpg
// VM461:41 url https://news.google.com/api/attachments/CC8iK0NnNWlWMFoxWnkxelpIbEVPR2gzVFJDb0FSaXJBaWdCTWdZUm1JS0hOZ1k=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iK0NnNWhOelJWWkVvMk5Wa3dkVVJQVFJDb0FSaXNBaWdCTWdhaEZJcXJGUW8=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iK0NnNXpNRXg2UjBWeGNVMU9VVGRLVFJDb0FSaXJBaWdCTWdhZEpJanNHUWs=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iL0NnNDBhMEozZW1wcWQzTXdRVmxRVFJDY0Foal9CQ2dLTWdrTmdvYjFQS1NwYlFF=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iL0NnNDFXamgwTlVwS1pVRTBkRmx5VFJERUF4aW1CU2dLTWdrSllZNElMdWd0S1FJ=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iJ0NnNUtiWEoyUzFORExXUjVaV1l5VFJDcEFSaXFBaWdCTWdNWnNBdw=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iK0NnNHlTRzFPTFZCaFJsaEdNeTFYVFJDZkF4amlCU2dLTWdhQkk0cHR0UVk=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iK0NnNXFabXhRV2poTVYyOURRMmhoVFJDZkF4ampCU2dLTWdZVmM0NHlOUVk=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iL0NnNTRMV0Z4TFRkbVUzcHpiMVpDVFJER0F4aWtCU2dLTWdrQlFKSmlvT2cwendF=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iK0NnNHhja1ZPVFV3ME1uQk1SRUZDVFJEZ0F4aUFCU2dLTWdhQkZvWUxFZ3M=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iK0NnNHlNVlk1VkcxVVZIRkhRbkp4VFJDZkF4ampCU2dLTWdZZGs1UnJNUWM=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iK0NnNVFjVVZxUm1vNWExOXFYMlEzVFJEckFoaUZCU2dLTWdZQkJKaU5LUWc=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iL0NnNVRiSGhQWmpkT2ExRnVlakJQVFJERUF4aW5CU2dLTWdrTkFaQ25sQ3A2Z3dN=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iL0NnNTJielZOYkdsdVlrZzVNMFpJVFJDZkF4ampCU2dLTWdrQklwejBsS3JJaGdJ=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iK0NnNHhjMUZ1WnpaamRuWTFlbTg1VFJEQUFoajBBeWdLTWdZTkNaQ25qZ3M=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iK0NnNXdhMDlmZDNCck1sbGlYMmREVFJDZkF4ampCU2dLTWdhQlU1VE5vUWc=-w200-h200-p-df-rw
// VM461:41 url https://news.google.com/api/attachments/CC8iK0NnNUxWbEZ3TTFaV09HdE9URVZLVFJDb0FSaXNBaWdCTWdZWllJakxJUWc=-w200-h200-p-df-rw
