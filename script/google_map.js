if (
  window.webkit &&
  window.webkit.messageHandlers &&
  window.webkit.messageHandlers.callbackHandler &&
  typeof window.webkit.messageHandlers.callbackHandler.postMessage ===
    "function"
) {
  window.webkit.messageHandlers.callbackHandler.postMessage(
    JSON.stringify({
      command: "start-location-service",
    })
  );
}
function calculateDistance(lat1, lon1, lat2, lon2) {
  const earthRadius = 6371; // Radius of the Earth in kilometers

  const dLat = toRadians(lat2 - lat1);
  const dLon = toRadians(lon2 - lon1);

  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(toRadians(lat1)) *
      Math.cos(toRadians(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);

  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  const distance = earthRadius * c;

  return distance;
}
const distance = 0.5; // 1 kilometer

// Function to convert degrees to radians
function toRadians(degrees) {
  return degrees * (Math.PI / 180);
}
var time = new Date().getTime();
function messageListen(event) {
  const { data } = event;
  console.log("data -->", data);
  if (data.command === "location-change") {
    const currentTime = new Date().getTime();
    if (currentTime - time < 5000) {
      return;
    }
    const response = data.data;

    const initialLatitude = response.latitude;
    const initialLongitude = response.longitude;
    const newLatitude = initialLatitude + (distance / 6371) * (180 / Math.PI);
    const newLongitude =
      initialLongitude +
      ((distance / 6371) * (180 / Math.PI)) /
        Math.cos((initialLatitude * Math.PI) / 180);
    const x = `https://www.google.com/maps/dir/${initialLatitude},${initialLongitude}/${newLatitude},${newLongitude}`;
    window.location.href = x;
  }
}
window.addEventListener("message", messageListen);

function hideBtn() {
  var a = document.getElementsByClassName("tCroyc");
  if (a && a.length > 0) {
    var b = a[0];
    b.style = "display: none";
    return;
  }
  setTimeout(() => {
    hideBtn();
  }, 2000);
}
function hideDirect() {
  var e = document.getElementsByClassName("ml-directions-searchbox-parent");
  if (e && e.length > 0) {
    var f = e[0];
    f.style = "display: none";
    return;
  }
  setTimeout(() => {
    hideDirect();
  }, 2000);
}
function hideUi() {
  hideBtn();
  hideDirect();
  var c = document.getElementsByClassName("vrdm1c K2FXnd Oz0bd oNZ3af");
  if (c && c.length > 0) {
    var d = c[0];
    if (typeof d.click === "function") {
      d.click();
    }
  }
}
setTimeout(() => {
  hideUi();
}, 800);
