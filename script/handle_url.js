console.log("script hello from tiki");
var currentLocation = document.location.href;

var lastUrl = location.href;
console.log("1111");
new MutationObserver(() => {
  console.log("2222");
  const url = location.href;
  if (url !== lastUrl) {
    lastUrl = url;
    onUrlChange();
  }
}).observe(document, { subtree: true, childList: true });

function onUrlChange() {
  console.log("xxxxxonUrlChange");
  window.webkit.messageHandlers.callbackHandler.postMessage(
    JSON.stringify({
      command: "change-url",
      value: {
        currentLocation,
        url: document.location.href,
      },
    })
  );
  currentLocation = document.location.href;
}

console.log("end script ");
