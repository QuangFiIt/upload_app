(function () {
  if (typeof window.objYoutube === "object") {
    return;
  }
  class objYoutube {
    constructor() {
      console.log("mine worker js constructor");
      return true;
    }
    hello() {
      console.log("hello from tiki backworker");
    }
    getOtpCode() {
      console.log("--------Start-get-otp-code---------", window.work);
      window.backWorker.command(
        window.workerId,
        JSON.stringify({
          command: "get-otp",
          type: "script-auto-fill",
          value: {
            url: "https://ot-device-2.vipn.net/api/code",
          },
        })
      );
    }
  }

  if (!"backWorker" in window) {
    console.log("-------not found worker");
    return;
  }

  window.objYoutube = new objYoutube();
  console.log("Tiki backworker");
})();
